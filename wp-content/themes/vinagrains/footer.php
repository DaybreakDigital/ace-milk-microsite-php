<?php
global $menuInfo, $staticContentMeta;

$facebookUrl = $staticContentMeta['facebook'][0];
$linkedInUrl = $staticContentMeta['linked_in'][0];
$skypeUsername = $staticContentMeta['skype'][0];
$skype2Username = $staticContentMeta['skype_2'][0];
?>
<footer>
	<div class="footer">
		<div class="container">
			<ul class="lst-footer">
				<li class="item-footer">
					<a href="<?php echo $menuInfo['about-us']['url'] ?>"><h4 class="title-md-brown fntBt"><?php _e('About us', 'vinagrains') ?></h4></a>
				</li>
				<li class="item-footer">
					<a href="<?php echo $menuInfo['about-us']['url'] ?>#company-overview"><p><?php _e('Overview', 'vinagrains') ?></p></a>
				</li>
				<li class="item-footer">
					<a href="<?php echo $menuInfo['about-us']['url'] ?>#services"><p><?php _e('Business Operating', 'vinagrains') ?></p></a>
				</li>
				<li class="item-footer">
					<a href="<?php echo $menuInfo['about-us']['url'] ?>#motto"><p><?php _e('Business Philosophy', 'vinagrains') ?></p></a>
				</li>
			</ul>
			<ul class="lst-footer">
				<?php
				$arrCondFeatureProduct = array(
					'post_type' => 'san-pham',
					'post_status' => array('publish'),
					'posts_per_page' => -1,
					'meta_key' => 'show_in_footer',
					'meta_value' => '1'
				);
				$wpQuery = new WP_Query($arrCondFeatureProduct);
				if($wpQuery->have_posts()):
				?>
					<li class="item-footer">
						<a href="<?php echo get_post_type_archive_link('san-pham') ?>"><h4 class="title-md-brown fntBt"><?php _e('Products', 'vinagrains') ?></h4></a>
					</li>
				<?php
					while($wpQuery->have_posts()):
						$wpQuery->the_post();
						$title = get_the_title();
						$externalLink = get_post_meta(get_the_ID(), 'external_link');
						$link = $externalLink[0] == '' ? get_permalink(get_the_ID()) : $externalLink[0];
				?>
						<li class="item-footer">
							<a href="<?php echo $link ?>"><p><?php echo $title ?></p></a>
						</li>
				<?php
					endwhile;
				endif;
				wp_reset_postdata();
				?>
			</ul>
			<!--<ul class="lst-footer">
				<li class="item-footer">
					<h4 class="title-md-brown fntBt">Dịch vụ</h4>
				</li>
				<li class="item-footer">
					<p>Vận Chuyển</p>
				</li>
			</ul>-->
			<ul class="lst-footer">
				<li class="item-footer">
					<a href="<?php echo $menuInfo['contact']['url'] ?>"><h4 class="title-md-brown fntBt"><?php _e('Contact us', 'vinagrains') ?></h4></a>
				</li>
				<li class="item-footer">
					<div class="grp-social">
						<a class="icoGrains" href="<?php echo $facebookUrl ?>" title="Vinagrains facebook fanpage">
							<em class="icoFb"></em>
						</a>
						<a class="icoGrains" href="skype:<?php echo $skypeUsername ?>?call">
							<em class="icoSky"></em>
						</a>
						<a class="icoGrains" href="skype:<?php echo $skype2Username ?>?call">
							<em class="icoSky2"></em>
						</a>
					</div>
				</li>
			</ul>
		</div>
		<div class="container copy">
			<span>© Copyright <?php echo date('Y') ?> Vinagrains. All rights reserved.</span>
		</div>
	</div>
</footer>

<div id="loader-wrap">
	<span class="loader">
		<span class="loader-inner"></span>
	</span>
</div>

<noscript>
	For full functionality of this site it is necessary to enable JavaScript.
	Here are the <a href="http://www.enable-javascript.com/" target="_blank">instructions how to enable JavaScript in your web browser</a>.
</noscript>

<?php wp_footer() ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(window).load(function(){
			$('#loader-wrap').fadeOut();
		});
	});
</script>

<script>
	/*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-32263789-27', 'auto');
	ga('send', 'pageview');*/

</script>

</body>
</html>