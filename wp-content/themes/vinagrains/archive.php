<?php

if(check_is_ajax()){
	uiwp_get_template('template/product-ajax.php');
	exit();
}

get_header();

global $menuInfo, $staticContentMeta, $curLang;

$bannerObj = wp_get_attachment_image_src($staticContentMeta['product_cover'][0], 'full');
$bannerUrl = $bannerObj[0];

$cateId = intval(get_query_var('cat'));
?>

<section class="top-banner">
	<article class="top-banner-holder banner-product" style="background: url('<?php echo $bannerUrl ?>') no-repeat center center;background-size: cover;">
		<h2 class="top-banner-holder-title"><?php _e('Products', 'vinagrains') ?></h2>
	</article>
</section>

<section class="product">

	<article class="product-filter">
		<h2 class="product-filter-title"><?php _e('category', 'vinagrains') ?></h2>

		<p class="product-filter-placeholder" data-value="" data-name="" callback="filterProduct">
			<?php
			if($cateId > 0):
				$cateObj = get_category($cateId);
				$cateName = $cateObj->name;
			?>
				<span><?php echo $cateName ?></span>
			<?php else: ?>
				<span><?php _e('Product Categories', 'vinagrains') ?></span>
			<?php endif; ?>
			<img class="product-filter-arrow" src="<?php echo LINKTHEME ?>images/whiteArrow.png" alt=""/>
		</p>

		<ul class="product-filter-holder">
			<?php
			$categories = get_categories();
			foreach($categories as $category):
			?>
				<li class="product-filter-item" data-value="<?php echo get_category_link($category->term_id) ?>"><?php echo $category->name ?></li>
			<?php endforeach; ?>
		</ul>
	</article>

	<article class="product-holder">
		<ul class="product-list">
			<?php
			$arrCondProduct = array(
				'post_type' => 'san-pham',
				'post_status' => array('publish'),
				'posts_per_page' => -1
			);
			if($cateId > 0){
				$arrCondProduct['cat'] = $cateId;
			}
			$wp_query = new WP_Query($arrCondProduct);
			if($wp_query->have_posts()):
				while($wp_query->have_posts()):
					$wp_query->the_post();
					$title = get_the_title();
					$imgObj = wp_get_attachment_image_src(get_post_thumbnail_id(), 'product-thumb');
					$imgUrl = $imgObj[0];
					$externalLink = get_post_meta(get_the_ID(), 'external_link');
			?>
					<li class="product-item">
						<?php if($externalLink[0] != ''): ?>
						<a href="<?php echo $externalLink[0] ?>">
						<?php else: ?>
						<a href="<?php echo get_permalink(get_the_ID()) ?>">
						<?php endif; ?>
							<img class="product-item-image" src="<?php echo $imgUrl ?>" alt="Product" title="<?php echo $title ?>"/>
							<p class="product-item-name"><?php echo $title ?></p>
						</a>
					</li>
			<?php
				endwhile;
			endif;
			wp_reset_postdata();
			?>
		</ul>
	</article>

</section>

<?php get_footer(); ?>