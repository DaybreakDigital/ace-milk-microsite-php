<?php
get_header();

global $menuInfo, $staticContentMeta, $curLang;

// About us
$pageId = $menuInfo['about-us']['id'];
$aboutUsMetaData = get_post_meta($pageId);
$introduction = $aboutUsMetaData['introduction'][0];
$shortDescription = $aboutUsMetaData['short_description'][0];
$slogan = $aboutUsMetaData['slogan'][0];

// Contact
$contactPageId = $menuInfo['contact']['id'];
$contactMetaData = get_post_meta($contactPageId);
/*$contactMapImgObj = wp_get_attachment_image_src($contactMetaData['_thumbnail_id'][0], 'full');
$contactMapUrl = $contactMapImgObj[0];*/
$address = apply_filters('the_content', $contactMetaData['address'][0]);
$totalEmails = $contactMetaData['emails'][0];
$totalPhones = $contactMetaData['phones'][0];
$mapObj = unserialize($contactMetaData['map'][0]);
$lat = $mapObj['lat'];
$lng = $mapObj['lng'];

?>

<div class="wrapper">
	<?php
	$arrSliderCond = array(
		'post_type' => 'home-slider',
		'post_status' => array('publish'),
		'posts_per_page' => -1
	);
	$wpQuery = new WP_Query($arrSliderCond);
	if($wpQuery->have_posts()):
		$totalSlide = $wpQuery->found_posts;
	?>
	<section class="cover">
		<div class="slider">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<?php
					while($wpQuery->have_posts()):
					$wpQuery->the_post();
					$title = get_the_title();
					$postMeta = get_post_meta(get_the_ID());
					$url = $postMeta['url'][0];
					$imgObj = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
					$imgUrl = $imgObj[0];
					?>
					<div class="swiper-slide" style="background: url('<?php echo $imgUrl ?>') no-repeat center center; background-size: cover;">
						<div class="caption">
							<h4 class="caption-title">
								<?php echo $title ?>
							</h4>
							<?php if($url != ''): ?>
							<a target="_blank" class="seemore" href="<?php echo $url ?>">
								<span><?php _e('Find out more', 'vinagrains') ?></span>
								<span class="arr"></span>
							</a>
							<?php endif; ?>
						</div>
					</div>
					<?php endwhile; ?>
				</div>

				<?php if($totalSlide > 1): ?>
				<div class="swiper-prev"></div>
				<div class="swiper-next"></div>
				<?php endif; ?>
			</div>
		</div>
		<div class="scrollMouse">
			<span class="a1"></span>
			<!--<span class="a2"></span>-->
			<!--<span class="a3"></span>-->
		</div>
	</section>
	<?php endif; ?>
	<section class="intro artiSection">
		<div class="container">
			<div class="intro-inner">
				<h4 class="title-md-brown"><?php echo $introduction ?></h4>
				<p><?php echo $shortDescription ?></p>
				<p class="text-uppercase fntBt-bryant"><?php echo $slogan ?></p>
				<a class="btn" href="<?php echo $menuInfo['about-us']['url'] ?>">
					<span class="text-btn"><?php _e('Overview', 'vinagrains') ?></span>
					<span class="arr"></span>
				</a>

			</div>
		</div>
	</section>

	<section class="secPrice">
		<div class="secPrice-inner">
			<div class="half-col bgBlue">
				<div class="table-price">
					<h4 class="title-md-brown fntBt"><?php _e('Price', 'vinagrains') ?></h4>
					<div class="ctn-table-price">
						<?php
						$arrayCondPriceTable = array(
							'post_type' => 'price-table',
							'post_status' => array('publish'),
							'posts_per_page' => -1
						);
						$wpQuery = new WP_Query($arrayCondPriceTable);
						if($wpQuery->have_posts()):
						?>
							<ul class="lst-product">
								<?php
								while($wpQuery->have_posts()):
									$wpQuery->the_post();
									$title = get_the_title();
									$postMeta = get_post_meta(get_the_ID());
									$unit = $postMeta['unit'][0];
								?>
									<li class="item-product">
										<p class="title-price">
											<span class="bgBlue"><?php echo $title ?></span>
											<span class="arrDown"></span>
										</p>
										<div class="block-price" style="">
											<?php
											$totalPrices = $postMeta['prices'][0];
											if($totalPrices > 0):
											?>
											<ul class="lst-price">
												<?php
												for($i = 0; $i < $totalPrices; $i++):
													$priceTitle = $postMeta['prices_'.$i.'_title'][0];
													$price = $postMeta['prices_'.$i.'_price'][0];
												?>
													<li class="item-price">
														<p class="label-qual">
															<span class="bgBlue">
																<?php echo $priceTitle ?>:
															</span>
														</p>
														<p class="price"><?php echo $price ?></p>
													</li>
												<?php
												endfor;
												?>
											</ul>
											<?php
											endif;
											?>
											<p>ĐVT: <?php echo $unit ?></p>
										</div>
									</li>
								<?php endwhile; ?>
							</ul>
						<?php
						endif;
						wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
			<div class="half-col">
				<div class="grp-service">
					<div class="box-product half-ver">
						<a class="bx-ver" href="<?php echo get_category_link(1) ?>">
							<div class="inner-bx-ver">
								<h4 class="title-md fntBt"><?php _e('Exported Products', 'vinagrains') ?></h4>
								<p class="seemore">
									<span><?php _e('View all', 'vinagrains') ?></span>
									<span class="arr"></span>
								</p>
							</div>
						</a>
					</div>
					<div class="box-service half-ver">
						<a class="bx-ver" href="<?php echo get_category_link(5) ?>">
							<div class="inner-bx-ver">
								<h4 class="title-md fntBt"><?php _e('Imported Products', 'vinagrains') ?></h4>
								<p class="seemore">
									<span><?php _e('View all', 'vinagrains') ?></span>
									<span class="arr"></span>
								</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="contact artiSection">
		<div class="contact-inner">
			<div class="container">
				<div class="half-col">
					<div class="contact-info">
						<div class="ctn-info">
							<h4 class="title-xs fntBt">
								<?php /*_e('Joint stock company', 'vinagrains') */?><!-- </br>-->
								<?php _e('VINAGRAINS CORPORATION', 'vinagrains') ?>
							</h4>
							<p><?php echo $address ?></p>

							<?php for($i = 0; $i < $totalEmails; $i++): ?>
								<p><?php echo $contactMetaData['emails_'.$i.'_email'][0] ?></p>
							<?php endfor; ?>

							<?php for($i = 0; $i < $totalPhones; $i++): ?>
								<p><?php echo $contactMetaData['phones_'.$i.'_phone'][0] ?></p>
							<?php endfor; ?>

							<a class="seemore" href="<?php echo $menuInfo['contact']['url'] ?>#contact-form">
								<span><?php _e('Send email', 'vinagrains') ?></span>
								<span class="arr"></span>
							</a>
						</div>
					</div>
				</div>
				<div class="half-col">
					<div class="map">
						<span class="inner-map"><img src="<?php echo LINKTHEME ?>images/map_<?php echo $curLang ?>.png" alt="<?php _e('VINAGRAINS CORPORATION', 'vinagrains') ?>" title="<?php _e('VINAGRAINS CORPORATION', 'vinagrains') ?>"></span>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>

<?php
get_footer();
?>