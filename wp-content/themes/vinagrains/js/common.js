$(document).ready(function(){

    $('.hamburger, .overlayPush').click (function(){
        pushMenu();
    });

    var swiper = new Swiper('.swiper-container', {
        nextButton: '.swiper-next',
        prevButton: '.swiper-prev',
        autoplay: 2000,
        effect : 'fade'
        //spaceBetween: 30
    });

    //select languages
    $('.currLang').click(function(e){
        e.stopPropagation()
        var $this = $(this);
        if($('.lst-lang').is(':hidden')){
            $this.parents('.nav-lst').find('.lst-lang').slideUp(80);
            $this.parent().find('.lst-lang').slideDown(120);
            $this.addClass('active')
        }else {
            $this.removeClass('active');
            $this.parent().find('.lst-lang').slideUp(80);
        }
    });

    $('.item-lang').click(function(e){
        e.stopPropagation()
        var curSlc = $('.currLang');
        var cur_click = $(this).html();
        var currentVal = $(this).attr('data-value');
        curSlc.html(cur_click);
        curSlc.attr('data-name',cur_click);
        curSlc.attr('data-value', currentVal);
        $(this).closest('.item-nav').find('.lst-lang').stop().slideUp(100);
        $(this).closest('.item-nav').find('.currLang').stop().removeClass('active');
        callbackFn($('.item-nav .currLang'));

    })


    $('body').click(function(){
        $('.lst-lang').stop().slideUp(80);
    })
    //END select languages

    $(window).load(function(){
        $(function(){
            $(window).resize(function(){
                var winH = $(window).height();
                var winW = $(window).width();
                var containerW = $('.container').outerWidth();
                var titlePriceH = $('.table-price>.title-md-brown').outerHeight(true);
                if(winW < 1183) {
                    $('.navBar').css({
                        'margin-top': 70,
                        'padding': 0,
                        'height': winH - 70,
                        'overflow-y': 'auto',
                        'overflow-x': 'hidden'
                    });
                    $('.table-price').removeAttr('style');
                }else {
                    $('.navBar').removeAttr('style');
                    $('.table-price').css({
                        'width': containerW/2,
                        'float' : 'right'
                    })
                }

                var heightSlider =$('.slider .swiper-slide').outerHeight();
                var heightCover =$('.top-banner-holder').outerHeight(true);
                var holderCover = $('.top-banner-holder');
                var holderSlider = $('.slider .swiper-slide');

                if (heightSlider >= winH && heightSlider <= winW){
                    $('.slider .swiper-slide').height(winH);
                }else if(heightSlider > winW){
                    $('.slider .swiper-slide').height(winW);
                }

                /*if (heightCover >= winH && heightCover <= winW){
                 $('.top-banner-holder').height(winH);
                 trace('height cover >= winH ' + true)
                 }else if(heightCover > winW){
                 $('.top-banner-holder').height(winW);
                 trace('height cover > winW ' + true)
                 }else {
                 $('.top-banner-holder').height('');
                 trace('height cover < winH ' + true)
                 }*/

                //heightslider
                    if(winH >= heightSlider) {
                        holderSlider.css('height','');
                    }else {
                        holderSlider.height(winH - 140)
                    }

                    if(winW <= heightSlider) {
                        holderSlider.height(winW);
                    }
                //END heightslider

                //heightCover
                    if(winH >= heightCover) {
                        holderCover.css('height','');
                    }else {
                        holderCover.height(winH - 140)
                    }

                    if(winW <= heightCover) {
                        holderCover.height(winW);
                    }
                //END heightCover



                if(winW < winH || winW < 768) {
                    $('.artiSection').css('height', '');
                }else {
                    $('.artiSection').css('height', winH);
                }

                if(winW < 768) {
                    $('.ctn-table-price').removeAttr('style');
                }else {
                    $('.ctn-table-price').css({
                        'height': winH - titlePriceH - 60
                    })
                }
            }).resize();
        })
    })



    //collapse price
    $('.title-price').click(function(e){
        e.stopPropagation();
        var $this = $(this);
        $(this).parents('.lst-product').find('.title-price').removeClass('active');
        if($this.parent().find('.block-price').is(':hidden')){
            $this.parents('.lst-product').find('.block-price').slideUp();
            $this.parent().find('.block-price').slideDown();
            $this.addClass('active');
        }else {
            $this.parent().find('.block-price').stop().slideUp();
            $this.removeClass('active');
        }
    })
    //END collapse price

    fixnav();
    //scrollMenu();

    $(window).on('load',function(){
        loadTopArticle();
    })

    $('.item-footer a').click(function(){
        offsetAnchor();
    })


// This will capture hash changes while on the page

    function offsetAnchor() {
        var navH = $('header').outerHeight();
        var marginTopNav = parseInt($('.nav').css('margin-top').replace('px',''));
        if(location.hash.length !== 0) {
            window.scrollTo(window.scrollX, window.scrollY - navH + marginTopNav);
        }
    }

    $(window).on("hashchange", function () {
        offsetAnchor();
    });

// This is here so that when you enter the page with a hash,
// it can provide the offset in that case too. Having a timeout
// seems necessary to allow the browser to jump to the anchor first.
    window.setTimeout(function() {
        offsetAnchor();
    }, 10);
    // The delay of 1 is arbitrary and may not always work right (although it did in my testing).


});


function loadTopArticle() {
    var pathnameHash = location.hash;
    var navH = $('header').outerHeight();
    var marginTopNav = $('.nav').css('margin-top').replace('px','');
    if(pathnameHash == true) {
        var targerOffset = $(pathnameHash).offset().top - navH + parseInt(marginTopNav);
        $(window).scrollTop(targerOffset);
        $(document).scrollTop();
    }
    //trace(targerOffset);
}


function fixnav(){
    $(window).scroll(function(){
        var scrollWindow = $(window).scrollTop();
        if(scrollWindow >= 1){
            $('.navMobile').addClass('fixNav');
            $('body').addClass('activeScroll_bdy');
        }else {
            $('.navMobile').removeClass('fixNav');
            $('body').removeClass('activeScroll_bdy');
        }
    });
}


function callbackFn(cal){
    var functions = cal.attr('callback');
    if(functions)
    {
        var fn = eval(functions);
        fn();
    }
}

function test(){
    console.log('abs');
}


function pushMenu(){
	var menuBtn = $('.hamburger');
	var pushLeft = $('.navMobile');
	var heightOvelay = $(document).height();
	menuBtn.toggleClass('activePush');
	$('.overlayPush').toggleClass('darkoverlay');
	$('body').toggleClass('push_body');
	pushLeft.toggleClass('pushMenu_show');
	var scrollWindow = $(window).scrollTop();
	if ($('.overlayPush').hasClass('darkoverlay')) {
		$('.overlayPush').css({'height': heightOvelay});
		$('.fixNav ').css({
			'transform': 'translate3d(0,'+ scrollWindow +'px, 0)'
		});
	}else {
		$('.overlayPush').css({'height':''});
		$('.fixNav ').css({
			'transform': 'translate3d(0,0,0)'
		});
	}
}

function trace(s){
    console.log(s)
}

function showPopUp(){
    $('.popup').stop().css({
        'visibility': 'visible',
        'opacity': 1
    });
}