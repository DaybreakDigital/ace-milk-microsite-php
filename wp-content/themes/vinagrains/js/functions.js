$(document).ready(function(){
    //$(document).on("click", 'a.num', faqPaging);
    $('#formContact').submit(submitContact);
    $("#vg_captcha").click(reloadCaptcha);
    $('#getDirection').click(getGoogleDirection);
});

function getGoogleDirection() {
    var start = $('#startPoint').val();
    var mode = parseInt($('input[name=mode]:checked').val());
    var TravelMode = '';

    if(start == ''){
        //alert(scvivocity_lang.scvivocity_require_start_point);
        alert('Vui lòng cung cấp địa chỉ điểm đi.');
        return false;
    }

    switch(mode) {
        case 1:
            TravelMode = google.maps.TravelMode.DRIVING;
            break;
        case 2:
            TravelMode = google.maps.TravelMode.TRANSIT;
            break;
        case 3:
            TravelMode = google.maps.TravelMode.WALKING;
            break;
        default:
            TravelMode = google.maps.TravelMode.DRIVING;
            break;
    }

    var request = {
        origin: start + " Hồ Chí Minh",
        destination: destination,
        travelMode: TravelMode
    };
    directionService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });

    return false;
}

function filterProduct(){
    var url = $('.product-filter-placeholder.active-slc').attr('data-value');

    if(url){
        $('#loader-wrap').show();

        $.get( url, function( response ) {
            if(response != ''){
                $('.product-list').html(response);
            }

            window.history.pushState({path:url},'',url);
            $('#loader-wrap').fadeOut();
        });
    }
    return false;
}

function submitContact(){
    var name = $.trim($('#formContact input[name=fullname]').val());
    var email = $.trim($('#formContact input[name=email]').val());
    var company = $.trim($('#formContact input[name=company]').val());
    var phone = $.trim($('#formContact input[name=phone]').val());
    var msg = $.trim($('#formContact textarea[name=msg]').val());

    if(!name){
        alert('Vui lòng nhập họ và tên.');
        return false;
    }

    if(!email){
        alert('Vui lòng nhập email.');
        return false;
    }else if(!isEmail(email)){
        alert('Email không hợp lệ.');
        return false;
    }

    if(!company){
        alert('Vui lòng nhập tên công ty.');
        return false;
    }

    if(!phone){
        alert('Vui lòng nhập số điện thoại.');
        return false;
    }else if(phone.length < 9 || phone.length > 13){
        alert('Số điện thoại không hợp lệ.');
        return false;
    }

    if(!msg){
        alert('Vui lòng nhập nội dung.');
        return false;
    }

    $('#loader-wrap').show();

    var formData = $('#formContact').serialize();
    var cbDepartment = $('.product-filter-placeholder');
    formData += '&contact=' + $.trim(cbDepartment.attr('data-name'));
    formData += '&to=' + $.trim(cbDepartment.attr('data-value'));

    $.post( site_url + "/wp-admin/admin-ajax.php", formData , function( data ) {
        if(data.status == 1){
            resetForm('formContact');
            //reloadCaptcha();
            alert('Cảm ơn.');
        }else if(data.status == '-1'){
            //reloadCaptcha();
            alert(data.message);
        }else {
            alert(data.message);
        }
    }, 'json').always(function(){
        $('#loader-wrap').fadeOut();
    });
    return false;
}

/**
 * User: Vinh Giang
 * Date: 17/04/2015
 * Time: 12:45 PM
 * Description: reload captcha
 */
function reloadCaptcha() {
    var captchaUrl = $("#vg_captcha_value").attr('src') + "?v=" + Math.random();
    $("#vg_captcha_value").attr("src", captchaUrl);
    return false;
}


/* Common script */

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function isEmail(s) {
	if (s.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}$/) != -1)
		return true;
	return false;
}

function resetForm(formID){
	$("#" + formID).each (function() { this.reset(); });
	return false;
}

function browseFile(uploadBtnID, formID, avatarField, avatarImgClass, uploadURL, loadImgURL){
	var btnUpload=$('#' + uploadBtnID);
	var ajaxUpload = new AjaxUpload(btnUpload, {
		action: uploadURL,
		name: 'file',
		onSubmit: function(file, ext){
			//showLoading();
			 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
				// extension is not allowed
				jAlert('Only accept image with JPG, PNG, GIF format');
				return false;
			}
		},
		onComplete: function(file, response){
			//On completion clear the status
			response = $.parseJSON(response);
			//Add uploaded file to list
			if(response.code === "success"){
				$('.' + avatarImgClass).html('<img src="' + loadImgURL + '/' + response.msg + '" />');
				$("#" + formID + " input[name=" + avatarField + "]").val(response.msg);
			} else{
				jAlert(response.msg);
			}
		}
	});
};

function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}

function checkExtensions(fileName){
    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
        return false;
    }
}

function isDate(txtDate){
    var currVal = txtDate;
    if(currVal == '')
       return false;
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{1,4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    if (dtArray == null)
        return false;
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];
    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay> 31)
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
        return false;
    else if (dtMonth == 2){
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap))
            return false;
    }
  return true;
}