/**
 * Created by Can on 10/30/2015.
 */

$(document).ready(function(){

    //set up filter list in product page;


    var Product_filterHolder = $(".product-filter-holder");
    var Product_filterPH = $(".product-filter-placeholder");
    var Product_filterList = $('.product-filter-item');
    var Product_filterLength = $('.product-filter-item').length;
    var Product_filterItemH = Product_filterPH.height();
    var Product_currentFilter = -1;
    var Product_filterArrow = $('product-filter-arrow');
    var Product_filterIsOpen = false;

    initProductFilter();
    setTimeout(resizeProductHolder, 100);

    function initProductFilter(){
        Product_filterList.each(function(id){
            var item = $(this);
            TweenMax.to(item,.1, {alpha:0, y:50, ease:Quint.easeOut})
            item.on("click", filterItemClick);
            item.attr("itemID", id);
        });
        Product_filterPH.on("click",doClickFilter);
    }

    function filterItemClick(e){
        var item = $(e.currentTarget);

        Product_filterPH.find('span').text(item.text());
        var dataValueItem = item.attr('data-value');
        Product_filterPH.attr({
            'data-name': item.text(),
            'data-value': dataValueItem
        });


        Product_filterCollapse();
        callbackFn(Product_filterPH);

        Product_filterIsOpen = !Product_filterIsOpen;
    }

    function doClickFilter(e){
        if(Product_filterIsOpen){
            Product_filterCollapse();
        }else{
            Product_filterOpen();
        }



        Product_filterIsOpen = !Product_filterIsOpen;
    }

    function Product_filterOpen(){
        TweenMax.to(Product_filterHolder, .5, {height:Product_filterItemH*Product_filterLength, ease:Quint.easeOut});
        Product_filterList.each(function(id){
            var item = $(this);
            TweenMax.killTweensOf(item)
            TweenMax.to(item,.5, {alpha:1, y:0, ease:Quint.easeOut, delay:id/10})
        });
        Product_filterPH.removeClass('active-slc');
    }

    function Product_filterCollapse(){
        TweenMax.to(Product_filterHolder, .5, {height:0, ease:Quint.easeOut, onComplete:function(){
            Product_filterList.each(function(e){
                var item = $(this);
                TweenMax.killTweensOf(item)
                TweenMax.to(item,.1, {alpha:0, y:50, ease:Quint.easeOut})
            });
        }});
        Product_filterPH.addClass('active-slc');
    }

    function trace(s){
        console.log(s);
    }


    $(window).resize(function(){
        resizeProductHolder();
    });

    function resizeProductHolder()
    {

            $('.product-list').css('width', 'auto'); //reset
            var windowWidth = $(document).width();
            var blockWidth = $('.product-item').outerWidth(true);
            var maxBoxPerRow = Math.floor(windowWidth / blockWidth);
            maxBoxPerRow = Math.min(maxBoxPerRow, 3);
            $('.product-list').width(maxBoxPerRow * blockWidth);

    }

});


