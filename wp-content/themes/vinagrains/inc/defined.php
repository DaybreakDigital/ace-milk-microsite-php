<?php
	
	define('LINKTHEME', get_template_directory_uri().'/');

	add_shortcode('getlinktheme', 'GetLinkTheme');
	function GetLinkTheme(){ return get_template_directory_uri().'/'; }

	add_shortcode('getsiteurl', 'GetSiteURL');
	function GetSiteURL(){ return site_url(); }
?>