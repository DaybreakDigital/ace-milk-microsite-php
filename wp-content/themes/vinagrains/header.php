<?php

global $menuInfo, $staticContentMeta, $curLang;

// Common
$curLang = ICL_LANGUAGE_CODE == 'vi' ? 'vi' : ICL_LANGUAGE_CODE;

// Static content
$staticContentPageId = apply_filters( 'wpml_object_id',  13 , 'page');
$staticContentMeta = get_post_meta($staticContentPageId);
$facebookUrl = $staticContentMeta['facebook'][0];
$linkedInUrl = $staticContentMeta['linked_in'][0];
$skypeUsername = $staticContentMeta['skype'][0];

?>
<!doctype html>
<!--[if IE 7]> <html class="no-js ie7" lang="<?php echo $curLang ?>"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8" lang="<?php echo $curLang ?>"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="<?php echo $curLang ?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="<?php echo $curLang ?>"> <!--<![endif]-->
<head>
	<title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo('name'); ?></title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="<?php echo $curLang ?>" />

	<!--[if lte IE 8]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<![endif]-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo('name'); ?>" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="author" content="Daybreak Digital" />

	<meta property="og:title" content="<?php echo get_the_title() ?>" />
	<meta property="og:description" content="<?php echo get_the_excerpt() ?>" />
	<meta property="og:url" content="<?php echo get_permalink() ?>" />
	<meta property="og:image" content="<?php echo $postThumbUrl ?>" />

	<link href="<?php echo LINKTHEME ?>images/favicon.png" rel="image_src" />
	<link href="<?php echo LINKTHEME ?>images/favicon.png" rel="icon" type="image/png" />

	<?php wp_head() ?>

	<!--[if !IE]><!--><script>if(/*@cc_on!@*/false){document.documentElement.className+=' ie10';}</script><!--<![endif]-->

	<script>
		var site_url = '<?php echo site_url() ?>';
	</script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.4";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<header>
	<div class="header">
		<div class="container">
			<div class="nav navMobile">
				<h1 class="logo"><a href="<?php echo home_url() ?>"><img src="<?php echo LINKTHEME ?>images/logo.png" alt="Vinagrains"></a></h1>
				<div class="box-navMb">
					<div class="navBar">
						<ul class="nav-lst">
							<li class="item-nav <?php echo is_home() ? 'active' : '' ?>">
								<a href="<?php echo home_url() ?>"><?php _e('Home', 'vinagrains') ?></a>
							</li>
							<li class="item-nav <?php echo $menuInfo['about-us']['class'] ?>">
								<a href="<?php echo $menuInfo['about-us']['url'] ?>"><?php _e('About us', 'vinagrains') ?></a>
							</li>
							<li class="item-nav">
								<a href="<?php echo get_post_type_archive_link('san-pham') ?>"><?php _e('Products', 'vinagrains') ?></a>
							</li>
							<li class="item-nav">
								<a href="<?php echo $menuInfo['contact']['url'] ?>"><?php _e('Contact us', 'vinagrains') ?></a>
							</li>
							<li class="item-nav">
								<span class="label-follow"><?php _e('Follow Us', 'vinagrains') ?></span>
								<div class="grp-social">
									<a target="_blank" class="icoGrains" href="<?php echo $facebookUrl ?>" title="Vinagrans facebook fanpage">
										<em class="icoFb"></em>
									</a>
<!--									<a target="_blank" class="icoGrains" href="--><?php //echo $linkedInUrl ?><!--" title="Vinagrans linked in">-->
<!--										<em class="icoIn"></em>-->
<!--									</a>-->
									<a class="icoGrains" href="skype:<?php echo $skypeUsername ?>?call">
										<em class="icoSky"></em>
									</a>
								</div>
							</li>
							<li class="item-nav">
                                    <span class="currLang active" data-value="" data-name="" callback="">
                                        <?php echo ICL_LANGUAGE_NAME ?>
                                    </span>
								<div class="lst-lang">
									<?php
									$listLang = apply_filters( 'wpml_active_languages', NULL, 'skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str' );
									foreach($listLang as $lang):
										$activeClass = ICL_LANGUAGE_CODE == $lang['language_code'] ? 'active' : '';
									?>
										<a class="item-lang" href="<?php echo $lang['url'] ?>" data-value=""><?php echo $lang['native_name'] ?></a>
									<?php
									endforeach;
									?>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="hamburger">
				<div class="line-grp">
					<span class="lineTop"></span>
					<span class="lineMid"></span>
					<span class="lineBottom"></span>
				</div>
			</div>

		</div>
	</div>
</header>