<?php

// Template name: contact

get_header();

global $menuInfo, $staticContentMeta, $curLang;

// Contact
$contactPageId = $menuInfo['contact']['id'];
$contactMetaData = get_post_meta($contactPageId);
/*$contactMapImgObj = wp_get_attachment_image_src($contactMetaData['_thumbnail_id'][0], 'full');
$contactMapUrl = $contactMapImgObj[0];*/
$address = apply_filters('the_content', $contactMetaData['address'][0]);
$totalEmails = $contactMetaData['emails'][0];
$totalPhones = $contactMetaData['phones'][0];
$mapObj = unserialize($contactMetaData['map'][0]);
$lat = $mapObj['lat'];
$lng = $mapObj['lng'];
?>

<section class="contactPage contact">
	<div class="contact-inner">
		<div class="container">
			<div class="half-col">
				<div class="contact-info">
					<div class="ctn-info">
						<h4 class="title-xs fntBt">
							<?php /*_e('Joint stock company', 'vinagrains') */?><!-- </br>-->
							<?php _e('VINAGRAINS CORPORATION', 'vinagrains') ?>
						</h4>
						<p><?php echo $address ?></p>

						<?php for($i = 0; $i < $totalEmails; $i++): ?>
							<p><?php echo $contactMetaData['emails_'.$i.'_email'][0] ?></p>
						<?php endfor; ?>

						<?php for($i = 0; $i < $totalPhones; $i++): ?>
							<p><?php echo $contactMetaData['phones_'.$i.'_phone'][0] ?></p>
						<?php endfor; ?>

						<a class="seemore" href="<?php echo $menuInfo['contact']['url'] ?>#find-map">
							<span><?php _e('View map', 'vinagrains') ?></span>
							<span class="arr"></span>
						</a>
					</div>
				</div>
			</div>
			<div class="half-col">
				<div class="map">
					<span class="inner-map"><img src="<?php echo LINKTHEME ?>images/map_<?php echo $curLang ?>.png" alt="<?php _e('VINAGRAINS CORPORATION', 'vinagrains') ?>" title="<?php _e('VINAGRAINS CORPORATION', 'vinagrains') ?>"></span>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="contact-form" class="info-contact">
	<div class="info-contact-inner">
		<form id="formContact" action="" method="post">
			<div class="topForm">
				<label class="label-form"></label>
				<div class="title-right-form">
					<div class="brown-line"></div>
					<h4 class="product-detail-header"><?php _e('Contact information', 'vinagrains') ?></h4>
				</div>
			</div>
			<fieldset class="control-form">
				<label class="label-form"><?php _e('Full Name', 'vinagrains') ?></label>
				<input name="fullname" class="ip-form" type="text">
			</fieldset>
			<fieldset class="control-form">
				<label class="label-form">Email</label>
				<input name="email" class="ip-form" type="text">
			</fieldset>
			<fieldset class="control-form">
				<label class="label-form"><?php _e('Company name', 'vinagrains') ?></label>
				<input name="company" class="ip-form" type="text">
			</fieldset>
			<fieldset class="control-form">
				<label class="label-form"><?php _e('Phone', 'vinagrains') ?></label>
				<input name="phone" class="ip-form" type="text">
			</fieldset>
			<fieldset class="control-form">
				<label class="label-form"><?php _e('Department', 'vinagrains') ?></label>
				<div class="grp-select-part">
					<p class="current-select product-filter-placeholder active-slc" data-value="<?php echo $staticContentMeta['departments_0_department_email'][0] ?>" data-name="<?php echo $staticContentMeta['departments_0_department_name'][0] ?>" callback="">
						<span><?php echo $staticContentMeta['departments_0_department_name'][0] ?></span>
					</p>
					<div class="select-part">
						<ul class="lst-part product-filter-holder">
							<?php
							$totalDepartments = $staticContentMeta['departments'][0];
							for($i = 0; $i < $totalDepartments; $i++):
								$title = $staticContentMeta['departments_'.$i.'_department_name'][0];
								$value = $staticContentMeta['departments_'.$i.'_department_email'][0];
							?>
								<li class="item-part product-filter-item" data-value="<?php echo $value ?>">
									<?php echo $title ?>
								</li>
							<?php endfor; ?>
						</ul>
					</div>
				</div>
			</fieldset>
			<fieldset class="control-form">
				<label class="label-form"><?php _e('Message', 'vinagrains') ?></label>
				<textarea name="msg" class="ip-form txtarea" rows="5"></textarea>
				<input type="hidden" name="action" value="vg_contact" >
				<?php echo wp_nonce_field("contact", "contact_form"); ?>
			</fieldset>
			<fieldset class="control-form">
				<label class="label-form"></label>
				<input class="btn" type="submit" value="<?php _e('Send', 'vinagrains') ?>">
			</fieldset>
		</form>
		<div id="find-map" class="find-map">
			<div class="topForm">
				<label class="label-form"></label>
				<div class="title-right-form">
					<h4 class="product-detail-header"><?php _e('Get direction', 'vinagrains') ?></h4>
				</div>
			</div>
			<form action="">
				<fieldset class="control-form">
					<label class="label-form"><?php _e('From', 'vinagrains') ?></label>
					<input id="startPoint" class="ip-form" type="text" placeholder="'<?php _e('House Number', 'vinagrains') ?>' '<?php _e('Street', 'vinagrains') ?>', '<?php _e('District', 'vinagrains') ?>'">
				</fieldset>
				<fieldset class="control-form">
					<label class="label-form"><?php _e('To', 'vinagrains') ?></label>
					<input readonly class="ip-form arrival" type="text" value="<?php echo $contactMetaData['address'][0] ?>">
				</fieldset>
				<fieldset class="control-form">
					<label class="label-form"></label>
					<input id="getDirection" class="btn" type="button" value="<?php _e('Get direction', 'vinagrains') ?>">
				</fieldset>
			</form>
			<div class="ctn-map">
				<div id="map"></div>
			</div>
		</div>
	</div>
</section>

<script>
	var map;
	var directionDisplay;
	var directionService = new google.maps.DirectionsService();
	var destination = new google.maps.LatLng('<?php echo $lat ?>', '<?php echo $lng ?>');

	function initialize() {
		directionsDisplay = new google.maps.DirectionsRenderer();
		var request = {
			origin: "33 Mạc Đĩnh Chi, Đa Kao, Ho Chi Minh, Vietnam",
			destination: destination,
			travelMode: google.maps.TravelMode.DRIVING
		};
		directionService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			}
		});

		var myOptions = {
			zoom: 16,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			//disableDefaultUI: true,
			//mapTypeControl: true
		};
		map = new google.maps.Map(document.getElementById('map'),myOptions);

		directionsDisplay.setMap(map);
	}

	$(document).ready(function(){
		initialize();
	})
</script>

<?php get_footer(); ?>