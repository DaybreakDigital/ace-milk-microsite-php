<?php

get_header();

global $menuInfo, $staticContentMeta, $curLang;

$bannerObj = wp_get_attachment_image_src($staticContentMeta['product_cover'][0], 'full');
$bannerUrl = $bannerObj[0];

$title = get_the_title();
$content = apply_filters('the_content', get_the_content());
$postMeta = get_post_meta(get_the_ID());
$quality = apply_filters('the_content', $postMeta['quality'][0]);

$category = get_the_category();
$cateName = $category[0]->name;
$cateId = $category[0]->term_id;
?>

<section class="top-banner">
	<article class="top-banner-holder banner-product" style="background: url('<?php echo $bannerUrl ?>') no-repeat center center;background-size: cover;">
		<h2 class="top-banner-holder-title"><?php _e('Products', 'vinagrains') ?></h2>
	</article>
</section>

<section class="product-detail">
	<div class="brown-line"></div>
	<ul class="product-detail-nav">
		<li><a href="<?php echo get_post_type_archive_link('product') ?>"><?php _e('Category', 'vinagrains') ?></a></li>
		<li><img src="<?php echo LINKTHEME ?>images/brownArrow.png" alt=""/></li>
		<li><a href="<?php echo get_category_link($cateId) ?>"><?php echo $cateName ?></a></li>
		<li><img src="<?php echo LINKTHEME ?>images/brownArrow.png" alt=""/></li>
		<li><?php echo $title ?></li>
	</ul>
	<h2 class="product-detail-header"><?php echo $title ?></h2>
	<div class="product-detail-content">
		<?php echo $content ?>
	</div>

	<div class="product-detail-qualification">
		<?php if($quality != ''): ?>
		<p><?php _e('Quality Standard', 'vinagrains') ?></p>
		<?php echo $quality ?>
		<?php endif; ?>

		<p><?php _e('For more information, please', 'vinagrains') ?> <a target="_blank" href="<?php echo $menuInfo['contact']['url'] ?>#contact-form"><span><?php _e('contact', 'vinagrains') ?></span></a></p>
	</div>
	<div class="brown-line"></div>
</section>

<?php get_footer(); ?>