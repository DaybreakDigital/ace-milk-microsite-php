<?php
$cateId = intval(get_query_var('cat'));

$arrCondProduct = array(
	'post_type' => 'san-pham',
	'post_status' => array('publish'),
	'posts_per_page' => -1
);
if($cateId > 0){
	$arrCondProduct['cat'] = $cateId;
}
$wp_query = new WP_Query($arrCondProduct);
if($wp_query->have_posts()):
	while($wp_query->have_posts()):
		$wp_query->the_post();
		$title = get_the_title();
		$imgObj = wp_get_attachment_image_src(get_post_thumbnail_id(), 'product-thumb');
		$imgUrl = $imgObj[0];
		$externalLink = get_post_meta(get_the_ID(), 'external_link');
		?>
		<li class="product-item">
			<?php if($externalLink[0] != ''): ?>
			<a href="<?php echo $externalLink[0] ?>">
			<?php else: ?>
			<a href="<?php echo get_permalink(get_the_ID()) ?>">
			<?php endif; ?>
				<img class="product-item-image" src="<?php echo $imgUrl ?>" alt="Product" title="<?php echo $title ?>"/>
				<p class="product-item-name"><?php echo $title ?></p>
			</a>
		</li>
	<?php
	endwhile;
else:
?>
	<p>Không có sản phẩm phù hợp.</p>
<?php
endif;
wp_reset_postdata();
?>