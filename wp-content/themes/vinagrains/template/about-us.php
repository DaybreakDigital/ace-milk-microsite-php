<?php

// Template name: about us

get_header();

global $menuInfo, $staticContentMeta, $curLang;

// About us
$pageId = $menuInfo['about-us']['id'];
$aboutUsMetaData = get_post_meta($pageId);
$aboutUsBannerObj = wp_get_attachment_image_src($aboutUsMetaData['about_us_image'][0], 'full');
$aboutUsBannerUrl = $aboutUsBannerObj[0];
$overview = $aboutUsMetaData['overview'][0];
$overviewImgObj = wp_get_attachment_image_src($aboutUsMetaData['overview_image'][0], 'full');
$overviewImgUrl = $overviewImgObj[0];
$services = $aboutUsMetaData['services'][0];
$servicesImgObj = wp_get_attachment_image_src($aboutUsMetaData['services_image'][0], 'full');
$servicesImgUrl = $servicesImgObj[0];
$motto = $aboutUsMetaData['motto'][0];

?>

	<section class="top-banner">
		<article class="top-banner-holder banner-about" style="background: url('<?php echo $aboutUsBannerUrl ?>') no-repeat center center; background-size: cover;">
			<h2 class="top-banner-holder-title"><?php _e('Overview', 'vinagrains') ?></h2>
		</article>
	</section>

	<section class="aboutUs">
		<article id="company-overview" class="aboutUs-top">
			<div class="col-left-about">
				<div class="brown-line"></div>
				<h4 class="product-detail-header"><?php _e('Overview our company', 'vinagrains') ?></h4>
				<p><?php echo $overview ?></p>
			</div>
			<div class="col-right-about">
				<span class="thumb-about"><img src="<?php echo $overviewImgUrl ?>" alt="About us" title="About us"></span>
			</div>
		</article>

		<article class="aboutUs-bottom">
			<div class="aboutUs-bottom-inner">
				<h4 id="services" class="product-detail-header"><?php _e('Business Operating', 'vinagrains') ?></h4>
				<p><?php echo $services ?></p>
				<span class="thumb-about"><img src="<?php echo $servicesImgUrl ?>" alt="<?php _e('Business Operating', 'vinagrains') ?>" title="<?php _e('Business Operating', 'vinagrains') ?>"></span>
				<div class="brown-line"></div>
				<h4 id="motto" class="product-detail-header"><?php _e('Business Philosophy', 'vinagrains') ?></h4>
				<p><?php echo $motto ?></p>
			</div>
		</article>

	</section>

<?php get_footer(); ?>