<?php
get_header();

global $menuInfo;
global $staticContentMeta, $curLang;

// Introduction
$introduction = apply_filters('the_content', $staticContentMeta['introduction'][0]);

// Video
$youtubeId = getYouTubeId($staticContentMeta['video'][0]);

// Benefit
$benefit = apply_filters('the_content', $staticContentMeta['benefit'][0]);

// Highlights
$highlights = apply_filters('the_content', $staticContentMeta['highlights'][0]);

// Nutrition
$nutrition = apply_filters('the_content', $staticContentMeta['nutrition'][0]);

// Ad image
$adImage = apply_filters('the_content', $staticContentMeta['ad_image'][0]);

// Categories & Packaging
$categoriesAndPackaging = apply_filters('the_content', $staticContentMeta['categories_and_packaging'][0]);

?>

<div class="wrapper">
	<section class="section_1 article-ace">
		<div class="content clearAfter">
			<div class="can_milk clearAfter">
				<img src="<?php echo LINKTHEME ?>images/milk_can.png" alt="milk can" title="milk can"/>
			</div>
			<div class="slogan_vid">
				<div class="slogan_description">
					<div class="slogan"><img src="<?php echo LINKTHEME ?>images/slogan.png" alt="slogan"/></div>
					<div class="description">
						<?php echo $introduction ?>
					</div>
					<span class="btn_check_it_out btn_callback" ><a href="#advantages">Khám Phá Ngay</a></span>
				</div>
				<div class="btn_vid">
					<div class="vid_utube">
						<?php if($youtubeId != '') :?>
							<iframe class="iframeVid" src="//www.youtube.com/embed/<?php echo $youtubeId ?>?rel=0&controls=0" frameborder="0" allowfullscreen></iframe>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<span class="make_img_vertical_bottom"></span>
		</div>
	</section>
	<section id="milkHard" class="section_2 article-ace">
		<div class="content">
			<h2 class="title clWhite">
				Sữa Đặc ACE
				<span class="description">
					Ngon Hơn Khi Kết Hợp Với
				</span>
				<span class="line-sm-bt"></span>
			</h2>
			<!--<span class="line"></span>-->
			<div class="mixinContent">
				<div class="mixin coffe">
					<div class="imgDetail"><img src="<?php echo LINKTHEME ?>images/coffe.png" alt="coffe" title="coffe"/></div>
					<div class="title">CAFE</div>
				</div>
				<div class="mixin smoothie">
					<div class="imgDetail"><img src="<?php echo LINKTHEME ?>images/smoothie.png" alt="smoothie" title="smoothie"/></div>
					<div class="title">SINH TỐ</div>
				</div>
				<div class="mixin icecream">
					<div class="imgDetail"><img src="<?php echo LINKTHEME ?>images/cream.png" alt="ice cream" title="ice cream"/></div>
					<div class="title">LÀM KEM</div>
				</div>
				<div class="mixin flan">
					<div class="imgDetail"><img src="<?php echo LINKTHEME ?>images/flan.png" alt="flan" title="flan"/></div>
					<div class="title">LÀM BÁNH</div>
				</div>
			</div>
		</div>
	</section>
	<section id="advantages" class="advantages article-ace">
		<div class="container">
			<h2 class="title-lg text-center clWhite">
				Điểm Mạnh Sản Phẩm ACE
				<span class="line-sm-bt"></span>
			</h2>
			<div class="clbt">
				<div class="fr tabs-advant">
					<ul class="lst-tabs clbt">
						<li class="item-tabs fl">
							<span class="title-itemTabs active">Lợi ích của dòng sản phẩm</span>
						</li>
						<li class="item-tabs fl">
							<span class="title-itemTabs">Đặc điểm nổi trội</span>
						</li>
						<li class="item-tabs fl">
							<span class="title-itemTabs">Thông tin dinh dưỡng</span>
						</li>
						<!--<li class="item-tabs fl">
							<span class="title-itemTabs">Hình ảnh quảng cáo</span>
						</li>-->
						<li class="item-tabs fl">
							<span class="title-itemTabs">Chủng loại và qui cách đóng gói</span>
						</li>
					</ul>
					<div class="container-tabs">
						<div class="ctn-tabs showTabs">
							<h4 class="title-xs-inner title-itemTabs">Lợi ích của dòng sản phẩm</h4>
							<div class="currentTabs">
								<?php echo $benefit ?>
							</div>
						</div>
						<div class="ctn-tabs">
							<h4 class="title-xs-inner title-itemTabs">Đặc điểm nổi trội</h4>
							<div class="currentTabs">
								<?php echo $highlights ?>
							</div>
						</div>
						<div class="ctn-tabs">
							<h4 class="title-xs-inner title-itemTabs">Thông tin dinh dưỡng</h4>
							<div class="currentTabs">
								<?php echo $nutrition ?>
							</div>
						</div>
						<!--<div class="ctn-tabs">
							<h4 class="title-xs-inner title-itemTabs">Hình ảnh quảng cáo</h4>
							<div class="currentTabs">
								<?php /*echo $adImage */?>
							</div>
						</div>-->
						<div class="ctn-tabs">
							<h4 class="title-xs-inner title-itemTabs">Chủng loại và qui cách đóng gói</h4>
							<div class="currentTabs">
								<?php echo $categoriesAndPackaging ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="fincf"></div>
	</section>
	<section id="distribute" class="distribute">
		<div class="container">
			<h2 class="title-lg text-center clGradient">
				Danh Sách Nhà Phân Phối
				<span class="line-sm-bt"></span>
			</h2>
			<div class="clbt">
				<div class="half-col">
					<div class="fanPage clbt">
						<div class="inner-fanPage fr">
							<div class="fb-page" data-href="https://www.facebook.com/SuaDacACE" data-width="328" data-height="477" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/SuaDacACE"><a href="https://www.facebook.com/SuaDacACE">ACE Milk - Sánh Quyện Mịn Màng</a></blockquote></div></div>
						</div>
					</div>
				</div>
				<div class="half-col">
					<div class="grp-distribute">

						<?php
						$regions = get_terms('region');
						if(!isset($regions->errors)):
							$firstRegionId = $regions[0]->term_id;
						?>
						<div class="select-distribute">
							<div class="inner-select">
								<p class="selected" callback="reloadDistributors"><?php echo $regions[0]->name ?></p>
								<span class="arr-select"></span>
							</div>
							<ul class="lst-select">
								<?php foreach($regions as $region): ?>
									<li class="item-select">
										<p class="txt-select" data-value="<?php echo $region->term_id ?>"><?php echo $region->name ?></p>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
						<div class="content-select">
							<div class="hold-select">
								<?php
								$arrCondDistributors = array(
									'post_type' => 'distributor',
									'post_status' => array('publish'),
									'posts_per_page' => -1,
									/*'tax_query' => array(
										array(
											'taxonomy' => 'region',
											'field'    => 'term_id',
											'terms'    => $firstRegionId,
										),
									)*/
								);
								$wpQuery = new WP_Query( $arrCondDistributors );
								if($wpQuery->have_posts()):
									while($wpQuery->have_posts()):
										$wpQuery->the_post();
										$distributorName = get_the_title();
										$description = get_the_excerpt();
										$postMeta = get_post_meta(get_the_ID());
										$phone = $postMeta['phone'][0];
										$fax = $postMeta['fax'][0];
								?>
										<div class="part-distribute">
											<h4 class="title-xs-inner clBlue fntBt"><?php echo $distributorName ?></h4>
											<p><?php echo $description ?></p>
											<ul class="number-contact clbt">
												<li class="fl">
													<span class="icoPhone"></span>
													<span><?php echo $phone ?></span>
												</li>
												<li class="fl">
													<span class="icoFax"></span>
													<span><?php echo $fax ?></span>
												</li>
											</ul>
										</div>
								<?php
									endwhile;
								endif;
								?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="contact" class="contact">
		<div class="container">
			<h2 class="title-lg text-center clGradient">
				Thông Tin Liên Hệ
				<span class="line-sm-bt"></span>
			</h2>
			<div class="clbt topContact">
				<div class="half-col">
					<?php
					$totalOffices = $staticContentMeta['offices'][0];
					for($i = 0; $i < $totalOffices; $i++):
					?>
						<div class="nameContact">
							<h3 class="title-xs fntBt clBlue"><?php echo $staticContentMeta['offices_'.$i.'_office_name'][0] ?></h3>
							<div class="">
								<?php echo apply_filters('the_content', $staticContentMeta['offices_'.$i.'_address'][0]) ?>
							</div>
						</div>
					<?php endfor; ?>
				</div>
				<div class="half-col">
					<div class="infoContact">
						<ul class="infoContact-lst clbt">
							<li class="infoContact-lst-item fl">
								<span class="icoPhone"></span>
								<div class="ctn-infoContact">
									<?php
									$totalPhones = $staticContentMeta['phones'][0];
									for($i = 0; $i < $totalPhones; $i++):
									?>
										<p><?php echo $staticContentMeta['phones_'.$i.'_phone'][0] ?></p>
									<?php endfor; ?>
								</div>
							</li>
							<li class="infoContact-lst-item fl">
								<span class="icoMail"></span>
								<div class="ctn-infoContact">
									<?php
									$totalEmails = $staticContentMeta['emails'][0];
									for($i = 0; $i < $totalEmails; $i++):
									?>
										<p><?php echo $staticContentMeta['emails_'.$i.'_email'][0] ?></p>
									<?php endfor; ?>
								</div>
							</li>
							<li class="infoContact-lst-item fl">
								<span class="icoFax"></span>
								<div class="ctn-infoContact">
									<?php
									$totalFaxes = $staticContentMeta['faxes'][0];
									for($i = 0; $i < $totalFaxes; $i ++):
									?>
										<p><?php echo $staticContentMeta['faxes_'.$i.'_fax'][0] ?></p>
									<?php endfor; ?>
								</div>
							</li>
							<li class="infoContact-lst-item fl">
								<span class="icoWorld"></span>
								<div class="ctn-infoContact">
									<?php
									$totalWebsites = $staticContentMeta['websites'][0];
									for($i = 0; $i < $totalWebsites; $i++):
									?>
										<a target="_blank" href="<?php echo $staticContentMeta['websites_'.$i.'_website'][0] ?>"><?php echo $staticContentMeta['websites_'.$i.'_website'][0] ?></a>
									<?php endfor; ?>
								</div>
							</li>
							<li class="infoContact-lst-item fl">
								<span class="icoFb"></span>
								<div class="ctn-infoContact">
									<?php
									$totalFanPages = $staticContentMeta['fanpages'][0];
									for($i = 0; $i < $totalFanPages; $i++):
										$fanPageUrl = $staticContentMeta['fanpages_'.$i.'_fanpage'][0];
										$fanPageName = explode('/', $fanPageUrl);
										$fanPageName = explode('?', $fanPageName[count($fanPageName) - 1]);
										$fanPageName = $fanPageName[0];
									?>
										<a target="_blank" class="clGrey" href="<?php echo $fanPageUrl ?>"><?php echo $fanPageName ?></a>
									<?php endfor; ?>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="main-contact clbt">
				<form id="form-contact" action="" method="POST">
					<div class="half-col">
						<div class="ctn-contact">
							<div class="form-control clbt">
								<div class="half-col">
									<fieldset class="field-form">
										<input class="ipContact" type="text" name="lastName" placeholder="Họ *"/>
										<label class="errSup">Vui lòng nhập Họ của bạn</label>
									</fieldset>
								</div>
								<div class="half-col">
									<fieldset class="field-form">
										<input class="ipContact" type="text" name="firstName" placeholder="Tên *"/>
										<label class="errSup">Vui lòng nhập Tên của bạn</label>
									</fieldset>
								</div>
							</div>
							<fieldset class="field-form">
								<input class="ipContact" type="email" name="email" placeholder="Email *"/>
								<label class="errSup">Vui lòng nhập Email của bạn</label>
							</fieldset>
							<fieldset class="field-form">
								<input class="ipContact numPhone" type="text" name="phone" placeholder="Số điện thoại *"/>
								<label class="errSup">Vui lòng nhập Số điện thoại của bạn</label>
							</fieldset>
						</div>
					</div>
					<div class="half-col">
						<div class="ctn-contact">
							<fieldset class="field-form">
								<textarea class="textarea" rows="6" name="msg" placeholder="Nội dung *"></textarea>
								<label class="errSup">Vui lòng nhập nội dung</label>
							</fieldset>
							<fieldset class="field-form">
								<?php if ( is_plugin_active( 'vg-captcha/vg-captcha.php' ) ) : ?>
									<a id="vg_captcha" href="#" title="Reload Captcha">
										<img id="vg_captcha_value" src="<?php echo get_permalink(getPageIDCaptcha()); ?>">
									</a>
									<input class="ipContact" name="captcha" type="text" placeholder="Mã bảo mật *">
								<?php endif; ?>
							</fieldset>
						</div>
					</div>
					<div class="clbt text-right">
						<input type="hidden" name="action" value="vg_contact" >
						<?php echo wp_nonce_field("contact", "contact_form"); ?>
						<input class="submitForm" type="submit" callback="submitContact" value="Gửi">
					</div>
				</form>
			</div>
		</div>
		<div class="thanks">
			<div class="content-thanks text-center">
				<div class="inner-thanks">
					<h2 class="clGradient title-lg">
						Thank you
						<span class="line-sm-bt"></span>
					</h2>
					<p>Cảm ơn bạn đã gửi thông tin đến Sữa Đặc ACE. Chúng tôi sẽ liên hệ bạn trong thời gian sớm nhất.</p>
					<span class="closeThanks"></span>
				</div>
			</div>
		</div>
	</section>
</div>

<?php
get_footer();
?>