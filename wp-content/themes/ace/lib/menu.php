<?php
/**
 * Created by PhpStorm.
 * User: vinhgiang
 * Date: 02/06/2015
 * Time: 6:22 CH
 */

function lang_object_ids($ids_array, $type = 'page') {
    $arrID = array();
    if(function_exists('icl_object_id')) {

        foreach ($ids_array as $name => $id) {
            //$xlat = icl_object_id($id, $type, true);
			$xlat = apply_filters( 'wpml_object_id', $id, $type );
            if(!is_null($xlat)) $arrID[$name] = $xlat;
        }

    } else {
        $arrID = $ids_array;
    }

    $res = array();
    foreach ($arrID as $name => $id) {
        if($id == 0) {
            $res[$name] = array(
                'id' => $id,
                'url' => home_url(),
                'class' => is_front_page() ? 'active' : ''
            );
        }else{
            $res[$name] = array(
                'id' => $id,
                'url' => get_permalink($id),
                'class' => is_page($id) ? 'active' : ''
            );
        }
    }

    return $res;
}

global $menuInfo;
$arrMenu = array(
    'front-page' => '0',
    'project-block' => 8,
    'house-block' => 10,
);
$menuInfo = lang_object_ids($arrMenu);