<?php
global $staticContentMeta;

?>
<footer>
	<div class="footer bgColor">
		<div class="container text-center">
			<div class="part-footer">
				<a href="../gioi-thieu"><h4 class="title-xs-inner fntBt">Giới Thiệu</h4></a>
				<a href="../gioi-thieu#company-overview">Về Công Ty</a>
				<a href="../gioi-thieu#services">Lĩnh Vực Hoạt Động</a>
				<a href="../gioi-thieu#motto">Phương Châm Kinh Doanh</a>
			</div>
			<div class="part-footer">
				<a href="../san-pham"><h4 class="title-xs-inner fntBt">Sản Phẩm</h4></a>
				<?php
				global $wpdb;
				$products = $wpdb->get_results("SELECT   us2yvf9vwh_posts.* FROM us2yvf9vwh_posts  INNER JOIN us2yvf9vwh_postmeta ON ( us2yvf9vwh_posts.ID = us2yvf9vwh_postmeta.post_id ) JOIN us2yvf9vwh_icl_translations t
							ON us2yvf9vwh_posts.ID = t.element_id
								AND t.element_type = CONCAT('post_', us2yvf9vwh_posts.post_type)  WHERE 1=1  AND (
  ( us2yvf9vwh_postmeta.meta_key = 'show_in_footer' AND CAST(us2yvf9vwh_postmeta.meta_value AS CHAR) = '1' )
) AND us2yvf9vwh_posts.post_type = 'san-pham' AND ((us2yvf9vwh_posts.post_status = 'publish')) AND ( ( t.language_code = 'vi' AND us2yvf9vwh_posts.post_type  IN ('post','page','acf-field-group','home-slider','price-table','san-pham' )  ) OR us2yvf9vwh_posts.post_type  NOT  IN ('post','page','acf-field-group','home-slider','price-table','san-pham' )  ) GROUP BY us2yvf9vwh_posts.ID ORDER BY us2yvf9vwh_posts.menu_order, us2yvf9vwh_posts.post_date DESC ", OBJECT );

				foreach($products as $product):
					$title = $product->post_title;
					$postType = $product->post_type;
					$slug = $product->post_name;
				?>
					<a href="../<?php echo $postType ?>/<?php echo $slug ?>"><?php echo $title ?></a>
				<?php endforeach; ?>
			</div>
			<!--<div class="part-footer">
				<h4 class="title-xs-inner fntBt">Dịch Vụ</h4>
				<a href="#">Vận Chuyển</a>
			</div>-->
			<div class="part-footer">
				<a href="#"><h4 class="title-xs-inner fntBt">Liên Hệ</h4></a>
				<div class="grp-social">
					<a target="_blank" class="icoAceMilk" href="https://www.facebook.com/SuaDacACE/?fref=ts"
					   title="Vinagrains facebook fanpage">
						<em class="icoFbFt"></em>
					</a>
					<a class="icoAceMilk" href="skype:khanhlinh06081993?call">
						<em class="icoSky"></em>
					</a>
					<a class="icoAceMilk" href="skype:trinh.nguyen0829?call">
						<em class="icoSky2"></em>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright text-center">
		<span>Một sản phẩm của Vina Grains</span>
	</div>
</footer>

<div id="loading">
	<span class="load loading"></span>
	<span class="load2 loading"></span>
	<span class="load3 loading"></span>
</div>

<noscript>
	For full functionality of this site it is necessary to enable JavaScript.
	Here are the <a href="http://www.enable-javascript.com/" target="_blank">instructions how to enable JavaScript in your web browser</a>.
</noscript>

<?php wp_footer() ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(window).load(function(){
			$('#loading').fadeOut();
		});
	});
</script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-32263789-27', 'auto');
	ga('send', 'pageview');

</script>

</body>
</html>