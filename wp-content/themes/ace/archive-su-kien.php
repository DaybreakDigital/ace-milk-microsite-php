<?php
get_header();

global $staticContentMeta, $curLang;

$eventDescription = $staticContentMeta['event_description'][0];
?>

<div class="wrapper">
	<section class="events">
		<div class="container main-events">
			<div class="head-events">
				<h2 class="title-events">
					Sự kiện
				</h2>
				<p><?php echo $eventDescription ?></p>
			</div>
			<div class="clbt content-events">
				<?php
				$page = intval($_GET['page']) == 0 ? '1' : intval($_GET['page']);
				$postsPerPage = 6;
				$arrEventsCond = array(
					'post_type' => 'su-kien',
					'post_status' => array('publish'),
					'posts_per_page' => $postsPerPage,
					'paged' => $page
				);
				$wpQuery= new WP_Query($arrEventsCond);
				if($wpQuery->have_posts()):
					while($wpQuery->have_posts()):
						$wpQuery->the_post();
						$title = get_the_title();
						$description = get_the_excerpt();
						$imgObj = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'event-thumb');
						$imgUrl = $imgObj[0];
						$date = get_post_time('d-m-Y');
						$weekDay = weekdayToVietnamese(get_post_time('l'));
				?>
						<div class="box-events">
							<?php if($imgUrl != ''): ?>
								<a href="<?php echo get_permalink(get_the_ID()) ?>" class="box-events-inner">
									<span class="box-events-thumb" style="background: url('<?php echo $imgUrl ?>') no-repeat center center; background-size: cover;"></span>
									<div class="box-events-title">
										<h4 class="title-box"><?php echo $title ?></h4>
									</div>
									<span class="seemore">
										Xem thêm
										<em class="icoMore"></em>
									</span>
								</a>
							<?php else : ?>
								<a href="<?php echo get_permalink(get_the_ID()) ?>" class="box-events-inner box-events-noImg">
									<div class="top-box-events">
										<p class="time-events"><?php echo $weekDay ?>, <?php echo $date ?></p>
										<h4 class="title-box"><?php echo $title ?></h4>
									</div>
									<div class="ctn-box-events">
										<p><?php echo $description ?></p>
									</div>
									<span class="seemore seemore-blue">
										Xem thêm
										<em class="icoMore"></em>
									</span>
								</a>
							<?php endif; ?>
						</div>
				<?php
					endwhile;
				endif;
				wp_reset_postdata();
				?>
			</div>

			<div class="pagination">
				<ul class="lst-page">
					<?php
					$totalRecords = $wpQuery->found_posts;
					//$totalPage = $wpQuery->max_num_pages;

					$objPage = new Pages();
					$objPage->Next = '';
					$objPage->Prev = '';
					// $objPage->First = $languages["first_page"];
					// $objPage->Last = $languages["last_page"];
					$objPage->prefixItem = '<li class="item-page">';
					$objPage->current = '<li class="item-page active"><a onclick="return false;" href="#">%page%</a></li>';
					$objPage->PrevClass = '';
					$objPage->NextClass = '';
					$objPage->ClassItem = 'num';

					$paging = $objPage->multipages($totalRecords, $postsPerPage, $page, '?page=%page%');

					print_r($paging);
					?>
				</ul>
			</div>

		</div>
	</section>
</div>

<?php
get_footer();
?>