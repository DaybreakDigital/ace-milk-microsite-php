<?php
include('inc/inc.php');
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

add_action('wp_enqueue_scripts', 'vinhgiang_style');
function vinhgiang_style() {

	wp_register_style('reset-style', get_template_directory_uri(). '/css/reset.css');
	wp_enqueue_style('reset-style');

	wp_register_style('common-style', get_template_directory_uri(). '/css/common.css');
	wp_enqueue_style('common-style');

	wp_register_style('main-style', get_template_directory_uri(). '/css/style.css');
	wp_enqueue_style('main-style');

	wp_register_style('editor-style', get_template_directory_uri(). '/css/editor-style.css');
	wp_enqueue_style('editor-style');

	global $wp_styles;

	wp_enqueue_style( 'html5', get_template_directory_uri() . '/js/html5.js' );
	$wp_styles->add_data( 'html5', 'conditional', 'lt IE 9' );

	// Embed after Jquery

	wp_enqueue_style( 'html5-IE', 'http://html5shiv.googlecode.com/svn/trunk/html5.js' );
	$wp_styles->add_data( 'html5-IE', 'conditional', 'IE' );

	wp_register_script('jquery-ui-script', get_template_directory_uri().'/js/jquery-ui.min.js', array('jquery'), '1.0', true);
	wp_enqueue_script('jquery-ui-script');

	wp_register_script('jquery.ui.touch-punch-script', get_template_directory_uri().'/js/jquery.ui.touch-punch.min.js', array('jquery'), '1.0', true);
	wp_enqueue_script('jquery.ui.touch-punch-script');

	wp_register_script('TweenMax-script', get_template_directory_uri().'/js/TweenMax.min.js', array('jquery'), '1.0', true);
	wp_enqueue_script('TweenMax-script');

	wp_register_script('common-script', get_template_directory_uri().'/js/common.js', array('jquery'), '1.0', true);
	wp_enqueue_script('common-script');

	wp_register_script('function-script', get_template_directory_uri().'/js/function.js', array('jquery'), '1.0', true);
	wp_enqueue_script('function-script');

	wp_register_script('functions-script', get_template_directory_uri().'/js/functions.js', array('jquery'), '1.0', true);
	wp_enqueue_script('functions-script');
}

// Change default jQuery
add_action('init', 'modify_jquery');
function modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_template_directory_uri().'/js/jquery-2.0.3.min.js', false, '2.0.3');
		wp_enqueue_script('jquery');
	}
}

// Your theme does not declare WooCommerce support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

/**
 * Add image size
 */
add_action( 'after_setup_theme', 'themeSetupImgSize' );
function themeSetupImgSize() {
	add_image_size( 'event-thumb', 488, 259, true ); // (cropped)
}

// Remove menu
add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){
    remove_menu_page( 'index.php' );                  //Dashboard
    //remove_menu_page( 'edit.php' );                   //Posts
    remove_menu_page( 'upload.php' );                 //Media
    //remove_menu_page( 'edit.php?post_type=page' );    //Pages
    remove_menu_page( 'edit-comments.php' );          //Comments
    //remove_menu_page( 'themes.php' );                 //Appearance
    //remove_menu_page( 'plugins.php' );                //Plugins
    //remove_menu_page( 'users.php' );                  //Users
    //remove_menu_page( 'tools.php' );                  //Tools
    //remove_menu_page( 'options-general.php' );        //Settings
}

// Hide donate form Post Types Order
add_action('admin_head', 'custom_colors');
function custom_colors() {
    echo '<style type="text/css">
            #cpt_info_box {
                display: none;
            }
         </style>';
}

// Get attachment data
function vg_get_attachment( $attachment_id ) {
    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}

// add_action('vg_top_slider_product', 'vg_top_slider_product_fcn');
// function vg_top_slider_product_fcn(){
// 	uiwp_get_template( 'template/top-slider-product.php' );
// }


// add_action("admin_menu", "setup_theme_admin_menus");
// function setup_theme_admin_menus() {
//     // add_submenu_page('themes.php',
//     //     'Front Page Elements', 'Front Page', 'manage_options',
//     //     'front-page-elements', 'theme_front_page_settings');
//     add_theme_page( 'Customize', 'Customize', 'edit_theme_options', 'customize.php' );
// }
// function theme_front_page_settings() {
//     echo "Hello, world!";
// }

/**
 * Add background controler to theme setting
*/
// $defaults = array(
// 	'default-color'          => '',
// 	'default-image'          => '',
// 	'wp-head-callback'       => '_custom_background_cb',
// 	'admin-head-callback'    => '',
// 	'admin-preview-callback' => ''
// );
// add_theme_support( 'custom-background', $defaults );


/*add_action('after_setup_theme', 'theme_setup_language_directory');
function theme_setup_language_directory (){
    load_theme_textdomain('scvivocity', get_template_directory(). '/languages');
}*/

/**
 * Register session
 */
if ( !session_id() )
    add_action( 'init', 'session_start' );


/**
 * Admin login page logo
*/
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo() {
	?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
			background-size: 90%;
			height: 170px;
			width: 220px;
			margin: -60px auto -27px;
        }
    </style>
	<?php
}

/**
 * Remove Wordpress Logo
*/
add_action('wp_before_admin_bar_render', 'vg_admin_bar_remove', 0);
function vg_admin_bar_remove() {
	global $wp_admin_bar;

	/* Remove their stuff */
	$wp_admin_bar->remove_menu('wp-logo');
}

/**
 *  Remove plugin update
 */
//remove_action('load-update-core.php','wp_update_plugins');
//add_filter('pre_site_transient_update_plugins','__return_null');

/**
 * Remove wordpress breadcrumbs
*/
add_action( 'init', 'vg_remove_wc_breadcrumbs' );
function vg_remove_wc_breadcrumbs() {
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}


/**
 * Add menu to theme
*/
/*add_theme_support('menus');
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
			'primary' => 'Primary Navigation',
			'secondary' => 'Secondary Navigation',
			'tertiary' => 'Tertiary Navigation'
		)
	);
}*/

/**
 * Get email template
 *
 * @param array $arrData
 * @param string $filePath
 *
 * @return mixed|string
 */
function getTemplateEmail($arrData , $filePath = '/email/contact.html'){
    $strContent = "";

	if(file_exists(get_template_directory().$filePath)){
		$fp = fopen(get_template_directory().$filePath,"r");
		$strContent = fread($fp, filesize(get_template_directory().$filePath));
		fclose($fp);

		foreach ($arrData as $key => $value) {
			$strContent = str_replace('{'.$key.'}', $value, $strContent);
		}
	}
	return $strContent;
}

if( !function_exists( 'uiwp_get_template' ) ) {
	/**
	* Retrieve a template file.
	*
	* @param string $path
	* @param mixed $var
	* @param bool $return
	* @return void
	* @since 1.0.0
	*/
	function uiwp_get_template( $path, $var = null, $return = false ) {
		$located = get_theme_root().'/'.get_template().'/'.$path;
		if ( $var && is_array( $var ) )
			extract( $var );

		if( $return )
			{ ob_start(); }

	// include file located
		include( $located );

		if( $return )
			return ob_get_clean();
	}
}

?>