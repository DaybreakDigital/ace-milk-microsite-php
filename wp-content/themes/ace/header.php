<?php

global $staticContentMeta, $curLang;

// Common
$curLang = defined(ICL_LANGUAGE_CODE) ? ICL_LANGUAGE_CODE : 'vi';

// Static content
$staticContentPageId = apply_filters( 'wpml_object_id',  13 , 'page');
$staticContentMeta = get_post_meta($staticContentPageId);

?>
<!doctype html>
<!--[if IE 7]> <html class="no-js ie7" lang="<?php echo $curLang ?>"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8" lang="<?php echo $curLang ?>"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="<?php echo $curLang ?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="<?php echo $curLang ?>"> <!--<![endif]-->
<head>
	<title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo('name'); ?></title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="<?php echo $curLang ?>" />

	<!--[if lte IE 8]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<![endif]-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo('name'); ?>" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="author" content="Daybreak Digital" />

	<meta property="og:title" content="<?php echo get_the_title() ?>" />
	<meta property="og:description" content="<?php echo get_the_excerpt() ?>" />
	<meta property="og:url" content="<?php echo get_permalink() ?>" />
	<meta property="og:image" content="<?php echo $postThumbUrl ?>" />

	<link href="<?php echo LINKTHEME ?>images/favicon.gif" rel="image_src" />
	<link href="<?php echo LINKTHEME ?>images/favicon.gif" rel="icon" type="image/gif" />

	<?php wp_head() ?>

	<!--[if !IE]><!--><script>if(/*@cc_on!@*/false){document.documentElement.className+=' ie10';}</script><!--<![endif]-->

	<script>
		var site_url = '<?php echo site_url() ?>';
	</script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.4";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<header>
	<div class="nav-menu clearAfter">
		<ul class="menu-main clearAfter">
			<li class="logo menu-li"><a href="<?php home_url() ?>"><img src="<?php echo LINKTHEME ?>images/logo.png" alt="logo" title="ace milk"/></a></li>
			<li class="menu-li"><a href="<?php echo home_url() ?>/#milkHard">Sữa Đặc ACE</a></li>
			<li class="menu-li"><a href="<?php echo home_url() ?>/#advantages">Điểm mạnh sản phẩm ACE</a></li>
			<li class="menu-li"><a href="<?php echo home_url() ?>/#distribute">Danh Sách Nhà Phân Phối</a></li>
			<li class="menu-li"><a class="<?php echo is_post_type_archive('su-kien') || is_singular('su-kien') ? 'active' : '' ?>" href="<?php echo get_post_type_archive_link('su-kien'); ?>">Sự kiện</a></li>
			<li class="menu-li"><a href="<?php echo home_url() ?>/#contact">Liên Hệ</a></li>
			<li class="hambuger menu-li">
				<div class="hambuger-ct">
					<span class="hambuger-ico"><span class="line"></span><span class="word">CLOSE</span></span>
				</div>
				<div class="menu-mobile">
					<div class="menu-item-rs"><a href="<?php echo home_url() ?>/#milkHard">Sữa Đặc ACE</a></div>
					<div class="menu-item-rs"><a href="<?php echo home_url() ?>/#advantages">Điểm mạnh sản phẩm ACE</a></div>
					<div class="menu-item-rs"><a href="<?php echo home_url() ?>/#distribute">Danh Sách Nhà Phân Phối</a></div>
					<div class="menu-item-rs"><a class="<?php echo is_post_type_archive('su-kien') || is_singular('su-kien') ? 'active' : '' ?>" href="<?php echo get_post_type_archive_link('su-kien'); ?>">Sự kiện</a></div>
					<div class="menu-item-rs"><a href="<?php echo home_url() ?>/#contact">Liên Hệ</a></div>
				</div>
			</li>
		</ul>
	</div>
	<p class="overlayPush"></p>
</header>