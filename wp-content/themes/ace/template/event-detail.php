<?php
get_header();

global $staticContentMeta, $curLang;

$eventDescription = $staticContentMeta['event_description'][0];

// Event detail
$title = get_the_title();
$weekDay = weekdayToVietnamese(get_post_time('l'));
$date = get_post_time('d-m-Y');
$imgObj = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), '');
$imgUrl = $imgObj[0];
$content = apply_filters('the_content', get_the_content());

?>

<div class="wrapper">
	<section class="events events-dts">
		<div class="container main-events">
			<div class="head-events">
				<h2 class="title-events">
					Sự kiện
				</h2>
				<p><?php echo $eventDescription ?></p>
			</div>
			<div class="details-events">
				<div class="details-events-inner">
					<div class="grpBtn">
						<a class="seemore btnBack" href="<?php echo get_post_type_archive_link('su-kien'); ?>">
							<em class="icoMore"></em>
							Quay về
						</a>
					</div>
					<div class="top-box-events">
						<p class="time-events"><?php echo $weekDay ?>, <?php echo $date ?></p>
						<h4 class="title-box"><?php echo $title ?></h4>
					</div>
					<?php if($imgUrl != ''): ?>
						<div class="box-events-thumb">
							<img src="<?php echo $imgUrl ?>" alt="event image" title="<?php echo $title ?>">
						</div>
					<?php endif; ?>
					<div class="ctn-details">
						<?php echo $content ?>
					</div>
				</div>
			</div>
			<?php
			$arrRelatedEvent = array(
				'post_type' => 'su-kien',
				'post_status' => array('publish'),
				'posts_per_page' => 2,
				'post__not_in' => array(get_the_ID())
			);
			$wpQuery = new WP_Query($arrRelatedEvent);
			if($wpQuery->have_posts()):
			?>
				<div class="relate">
					<h4 class="title-xs-inner">Sự kiện liên quan</h4>
					<div class="sec-relate content-events clbt">
						<?php
						while($wpQuery->have_posts()):
							$wpQuery->the_post();
							$title = get_the_title();
							$description = get_the_excerpt();
							$imgObj = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'event-thumb');
							$imgUrl = $imgObj[0];
							$weekDay = weekdayToVietnamese(get_post_time('l'));
							$date = get_post_time('d-m-Y');
						?>
							<div class="box-events">
								<?php if($imgUrl != ''): ?>
									<a href="<?php echo get_permalink(get_the_ID()) ?>" class="box-events-inner">
										<span class="box-events-thumb" style="background: url('<?php echo $imgUrl ?>') no-repeat center center;background-size: cover;"></span>
										<div class="box-events-title">
											<h4 class="title-box"><?php echo $title ?></h4>
										</div>
										<span class="seemore">
											Xem thêm
											<em class="icoMore"></em>
										</span>
									</a>
								<?php else: ?>
									<a href="<?php echo get_permalink(get_the_ID()) ?>" class="box-events-inner box-events-noImg">
										<div class="top-box-events">
											<p class="time-events"><?php echo $weekDay ?>, <?php echo $date ?></p>
											<h4 class="title-box"><?php echo $title ?></h4>
										</div>
										<div class="ctn-box-events">
											<p><?php echo $description ?></p>
										</div>
										<span class="seemore seemore-blue">
											Xem thêm
											<em class="icoMore"></em>
										</span>
									</a>
								<?php endif; ?>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			<?php
			endif;
			wp_reset_postdata();
			?>
		</div>
	</section>
</div>

<?php get_footer(); ?>