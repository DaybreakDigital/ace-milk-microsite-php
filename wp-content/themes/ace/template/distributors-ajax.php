<?php
$regionId = intval($_POST['id']);

$arrCondDistributors = array(
	'post_type' => 'distributor',
	'post_status' => array('publish'),
	'posts_per_page' => -1,
	'tax_query' => array(
		array(
			'taxonomy' => 'region',
			'field'    => 'term_id',
			'terms'    => $regionId,
		),
	)
);
$wpQuery = new WP_Query( $arrCondDistributors );
if($wpQuery->have_posts()):
	while($wpQuery->have_posts()):
		$wpQuery->the_post();
		$distributorName = get_the_title();
		$description = get_the_excerpt();
		$postMeta = get_post_meta(get_the_ID());
		$phone = $postMeta['phone'][0];
		$fax = $postMeta['fax'][0];
		?>
		<div class="part-distribute">
			<h4 class="title-xs-inner clBlue fntBt"><?php echo $distributorName ?></h4>
			<p><?php echo $description ?></p>
			<ul class="number-contact clbt">
				<li class="fl">
					<span class="icoPhone"></span>
					<span><?php echo $phone ?></span>
				</li>
				<li class="fl">
					<span class="icoFax"></span>
					<span><?php echo $fax ?></span>
				</li>
			</ul>
		</div>
	<?php
	endwhile;
endif;
?>