$(document).ready(function(){
    //$(document).on("click", 'a.num', faqPaging);

    $("#vg_captcha").click(reloadCaptcha);
});

function submitContact(){
    $('#loading').show();
    $.post( site_url + "/wp-admin/admin-ajax.php", $('#form-contact').serialize() , function( data ) {
        $('#loading').fadeOut();
        if(data.status == 1){
            resetForm('form-contact');
            reloadCaptcha();
            $('.thanks').stop().css({
             'opacity': 1,
             'visibility': 'visible'
             });
        }else if(data.status == '-1'){
            reloadCaptcha();
            alert(data.message);
        }else {
            alert(data.message);
        }
    }, 'json');
    return false;
}

function reloadDistributors() {
    var value = $('.selected').attr('data-value');

    if(isNaN(value) || value <= 0){
        alert('Lỗi. Vui lòng tải lại trang.');
    }else {
        $('#loading').fadeIn();
        $.post( site_url + "/wp-admin/admin-ajax.php", {"action": "vg_distributor_ajax_fillter", "id": value} , function( data ) {
            if(data != ''){
                $('.hold-select').html(data);
                $('#loading').fadeOut();
            }
        });
    }
    return false;
}

/**
 * User: Vinh Giang
 * Date: 17/04/2015
 * Time: 12:45 PM
 * Description: reload captcha
 */
function reloadCaptcha() {
    var captchaUrl = $("#vg_captcha_value").attr('src') + "?v=" + Math.random();
    $("#vg_captcha_value").attr("src", captchaUrl);
    return false;
}


/* Common script */

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function isEmail(s) {
	if (s.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}$/) != -1)
		return true;
	return false;
}

function resetForm(formID){
	$("#" + formID).each (function() { this.reset(); });
	return false;
}

function browseFile(uploadBtnID, formID, avatarField, avatarImgClass, uploadURL, loadImgURL){
	var btnUpload=$('#' + uploadBtnID);
	var ajaxUpload = new AjaxUpload(btnUpload, {
		action: uploadURL,
		name: 'file',
		onSubmit: function(file, ext){
			//showLoading();
			 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
				// extension is not allowed
				jAlert('Only accept image with JPG, PNG, GIF format');
				return false;
			}
		},
		onComplete: function(file, response){
			//On completion clear the status
			response = $.parseJSON(response);
			//Add uploaded file to list
			if(response.code === "success"){
				$('.' + avatarImgClass).html('<img src="' + loadImgURL + '/' + response.msg + '" />');
				$("#" + formID + " input[name=" + avatarField + "]").val(response.msg);
			} else{
				jAlert(response.msg);
			}
		}
	});
};

function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}

function checkExtensions(fileName){
    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
        return false;
    }
}

function isDate(txtDate){
    var currVal = txtDate;
    if(currVal == '')
       return false;
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{1,4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    if (dtArray == null)
        return false;
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];
    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay> 31)
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
        return false;
    else if (dtMonth == 2){
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap))
            return false;
    }
  return true;
}