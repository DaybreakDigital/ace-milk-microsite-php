<?php
get_header(); ?>

<link rel="stylesheet" href="<?php echo LINKTHEME; ?>css/detail.css">
<section id="search">
    <div class="breadcrumb">
        <ul>
            <li><a href="<?php echo home_url() ?>">Home</a></li>
            <li>/</li>
            <li>Tìm kiếm</li>
        </ul>
    </div>

    <div class="body">
        <div class="left">
            <h3>KẾT QUẢ TÌM KIẾM</h3>
            <p class="keywords">“<?php echo get_search_query(); ?>”</p>
            <div class="line"></div>
            <div class="search-result">
                <?php
                if ( have_posts() ) :
                    while ( have_posts() ) :
                        the_post();
                        $description = substring(get_the_content(), 200);
                ?>
                        <div class="item">
                            <a href="<?php echo get_permalink() ?>" alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>"><h3><?php echo get_the_title() ?></h3></a>
                            <p><?php echo $description ?></p>
                        </div>
                <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
        <span class="vertical-line"></span>
        <?php  uiwp_get_template( 'template/right-banner.php', $atts); ?>
    </div>
</section>

<?php get_footer(); ?>
