<?php

/**

 * Created by PhpStorm.

 * User: vinhgiang

 * Date: 20/03/2015

 * Time: 2:14 CH

 */

add_action('wp_ajax_nopriv_vg_contact', 'submitContact');
add_action('wp_ajax_vg_contact', 'submitContact');
function submitContact(){

    if(wp_verify_nonce($_POST['contact_form'], 'contact')){

        $response = array(
            'status' => 0
        );

        $firstName = wp_strip_all_tags($_POST['firstName']);
		$lastName = wp_strip_all_tags($_POST['lastName']);
        $email = wp_strip_all_tags($_POST['email']);
        $phone = wp_strip_all_tags($_POST['phone']);
        $msg = wp_strip_all_tags($_POST['msg']);
        $captcha = wp_strip_all_tags($_POST['captcha']);

        if(!$firstName){
            $response['message'] = 'Error code: 0x001';
        }else if(strlen($firstName) >= 50){
            $response['message'] = 'Your name must be less than 50 character.';
        }

		if(!$lastName){
			$response['message'] = '0x001';
		}else if(strlen($lastName) >= 50){
			$response['message'] = 'Your name must be less than 50 character.';
		}

        if(!$email){
            $response['message'] = 'Error code: 0x010';
        }else if(!is_email($email)){
            $response['message'] = 'Error code: 0x007';
        }else if(strlen($email) > 200){
            $response['message'] = 'Your email address must be less than 200 character.';
        }

		if(!$phone){
			$response['message'] = 'Error code: 0x005';
		}else if(strlen($phone) < 9 || strlen($phone) > 13){
            $response['message'] = 'Error code: 0x006';
        }

        if(!$msg){
            $response['message'] = 'Error code: 0x013';
        }

        if(is_plugin_active('vg-captcha/vg-captcha.php')){
            if($captcha != $_SESSION['vg_captcha']){
                $response['message'] = 'Your captcha code is not valid.'.$captcha.' - '.$_SESSION['vg_captcha'];
                $response['status'] = -1;
            }
        }

        if(empty($response['message'])){

            $arrData = array(
                'first_name' => $firstName,
				'last_name' => $lastName,
                'email' => $email,
                'phone' => $phone,
                'message' => $msg,
                'ip' => getClientIP()
            );

            global $wpdb;

            $wpdb->insert($wpdb->prefix.'contact' , $arrData);
            $newsletterId = $wpdb->insert_id;
            if($newsletterId > 0){
                $response['status'] = 1;

				$arrData['website'] = site_url();

				$to = 'admin@vinagrains.com';
				//$to = 'vinh.giang@daybreak.am';
				$subject = '[contact] New message';
				$body = getTemplateEmail($arrData, '/email/contact.html');
				$headers = array('Content-Type: text/html; charset=UTF-8');

				wp_mail( $to, $subject, $body, $headers );
            }else{
                $response['message'] = 'System error! Please try again.';
            }
        }
        print_r(json_encode($response)); exit;
    }
}

add_action('wp_ajax_nopriv_vg_order', 'submitOrder');
add_action('wp_ajax_vg_order', 'submitOrder');
function submitOrder(){

    if(wp_verify_nonce($_POST['order_form'], 'order')){

        $response = array(
            'status' => 0
        );

        $name = wp_strip_all_tags($_POST['fullname']);
        $phone = wp_strip_all_tags($_POST['cellphone']);
		$email = wp_strip_all_tags($_POST['email']);
        $address = wp_strip_all_tags($_POST['address']);
		$district = wp_strip_all_tags($_POST['district']);
		$province = wp_strip_all_tags($_POST['province']);
        $quantity = intval($_POST['quantity']) + intval($_SESSION['cart']);

        if(!$name){
            $response['message'] = 'Please enter your name.';
        }else if(strlen($name) > 200){
            $response['message'] = 'Your name must be less than 200 character.';
        }

        if(strlen($phone) > 13){
            $response['message'] = 'Your phone number must be less than 13 character.';
        }

		if(!$email){
			$response['message'] = 'Please enter your email address.';
		}else if(strlen($email) > 200){
			$response['message'] = 'Your email must be less than 200 character.';
		}else if(!is_email($email)) {
			$response['message'] = 'Your email is not valid.';
		}

        if(!$address){
            $response['message'] = 'Please enter your address.';
        }else if(strlen($address) > 200){
            $response['message'] = 'Your address must be less than 200 character.';
        }

		if(!$district){
			$response['message'] = 'Please enter your district.';
		}
		if(!$province){
			$response['message'] = 'Please select your province.';
		}

        if($quantity <= 0) {
            $response['message'] = 'Your cart is empty.';
        }


        if(empty($response['message'])){

            $arrData = array(
                'post_type' => 'order-type',
                'post_title' => $name
            );

            $orderId = wp_insert_post($arrData);

            update_field('name', $name, $orderId);
            update_field('phone', $phone, $orderId);
			update_field('email', $email, $orderId);
            update_field('address', $address, $orderId);
			update_field('district', $district, $orderId);
			update_field('province', $province, $orderId);
            update_field('quantity', $quantity, $orderId);
			update_field('ip', getClientIP(), $orderId);

            if($orderId > 0){

                $arrOrderData = array(
					'id' => $orderId,
                    'name' => $name,
                    'website' => site_url(),
                    'phone' => $phone,
					'email' => $email,
                    'address' => $address,
					'district' => $district,
					'province' => $province,
                    'quantity' => $quantity,
                );

				$_SESSION['orderInfo'] = $arrOrderData;

                $to = 'support@myvitajoint.vn';
                $subject = '[order] New order';
                $body = getTemplateEmail($arrOrderData, '/email/order.html');
                $headers = array('Content-Type: text/html; charset=UTF-8');

                wp_mail( $to, $subject, $body, $headers );

                $_SESSION['cart'] = 0;

                $response['status'] = 1;
            }else{
                $response['message'] = 'System error! Please try again.';
            }
        }
        print_r(json_encode($response)); exit;
    }
}

add_action('wp_ajax_nopriv_vg_distributor_ajax_fillter', 'distributorsAjaxFilter');
add_action('wp_ajax_vg_distributor_ajax_fillter', 'distributorsAjaxFilter');
function distributorsAjaxFilter(){
    uiwp_get_template( 'template/distributors-ajax.php' ); exit;
}
