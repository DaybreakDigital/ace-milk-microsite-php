<?php
/**
* Plugin Name: VG_Captcha
* Description:
* Version: 1.0
* Author: Vinh Giang
* Author URI: giangcamvinh@gmail.com
*/

//define('_DIR',  plugins_url('/' , __FILE__ ));
define('_DIR',  plugin_dir_path(__FILE__ ));

include('captcha.php');

register_activation_hook(__FILE__, 'installCaptcha');
function installCaptcha() {
    global $wpdb;
    $the_page_title = 'Captcha';
    $the_page_name = 'captcha';

    $the_page = get_page_by_title( $the_page_title );
    if ( ! $the_page ) {

        // Create post object
        $arrPage = array();
        $arrPage['post_title'] = $the_page_title;
        $arrPage['post_content'] = "[vg_captcha]";
        $arrPage['post_status'] = 'publish';
        $arrPage['post_type'] = 'page';
        $arrPage['comment_status'] = 'closed';
        $arrPage['ping_status'] = 'closed';
        $arrPage['post_category'] = array(1);

        // Insert the post into the database
        $pageId = wp_insert_post( $arrPage );

    }else{
    	$pageId = $the_page->ID;
    }
    delete_option( 'VGPageID' );
    add_option( 'VGPageID', $pageId );
}

add_shortcode( 'vg_captcha', 'showVgCaptcha' );
function showVgCaptcha( $atts ) {

	$captcha = new SimpleCaptcha();

	// OPTIONAL Change configuration...
	//$captcha->wordsFile = 'words/es.php';
	//$captcha->session_var = 'secretword';
	//$captcha->imageFormat = 'png';
	//$captcha->lineWidth = 3;
	//$captcha->scale = 3; $captcha->blur = true;
	//$captcha->resourcesPath = "/var/cool-php-captcha/resources";

	// OPTIONAL Simple autodetect language example
	/*
	if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
	    $langs = array('en', 'es');
	    $lang  = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	    if (in_array($lang, $langs)) {
	        $captcha->wordsFile = "words/$lang.php";
	    }
	}
	*/

	ob_clean();
	$captcha->resourcesPath = _DIR.'resources';
	$captcha->height = 76;
	$captcha->session_var = "vg_captcha";
	$captchaImg = $captcha->CreateImage();
	return $captchaImg;
}

add_action('wp_ajax_nopriv_reload_vg_captcha', "reloadVgCaptcha");
add_action('wp_ajax_reload_vg_captcha', "reloadVgCaptcha");
function reloadVgCaptcha(){
	showVgCaptcha();
}

function getPageIDCaptcha(){
	return get_option('VGPageID');
}

?>