<?php
    $perpage = 50;
    $maxPage=999999999;
    $strQuery = '';
    $page = isset( $_GET['paged'] ) ? intval( $_GET['paged'] ) : 1;

	$tablePrefix = get_current_blog_id() == 1 ? $wpdb->prefix : $wpdb->prefix.'_'.get_current_blog_id().'_';

    $resultQueryCount = $wpdb->get_results('SELECT COUNT(id) as count FROM '.$tablePrefix.'contact');
    $totalPage = ceil($resultQueryCount[0]->count / $perpage); 

    $paging = paginate_links( array(
            'base' => str_replace( $maxPage, '%#%', esc_url( get_pagenum_link( $maxPage ) ) ),
            'format' => '&page=%#%',
            'current' => max( 1, $page ),
            'total' => $totalPage,
            'prev_text' => '<<',
            'next_text' => '>>'
        ) );
    $results = $wpdb->get_results( "SELECT * FROM ".$tablePrefix."contact LIMIT ".($page-1) * $perpage." , ".$perpage * $page." " , ARRAY_A);
?>

<form method="post" action="?page=<?php echo $_GET['page']; ?>">
	<p class="search-box">
		<label class="screen-reader-text" for="post-search-input">Search Posts:</label>
		<input type="search" id="post-search-input" name="s" value="">
		<input type="submit" name="" id="search-submit" class="button" value="Search Posts">
	</p>

	<table cellspacing="0" class="wp-list-table widefat fixed store-locator">
      <thead>
        <tr>
            <th style="" class="manage-column column-cb check-column" id="cb" scope="col"><input type="checkbox"></th>
            <th style="" class="manage-column column-name" id="name-form" scope="col">First Name<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-name" id="name-form" scope="col">Last Name<span class="sorting-indicator"></span></th>
			<th style="" class="manage-column column-name" id="name-form" scope="col">Full Name<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-name" id="name-form" scope="col">Email<span class="sorting-indicator"></span></th>
			<th style="" class="manage-column column-name" id="name-form" scope="col">Company<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-name" id="name-form" scope="col">Phone<span class="sorting-indicator"></span></th>
            <!--<th style="" class="manage-column column-name" id="name-form" scope="col">Subject<span class="sorting-indicator"></span></th>-->
            <th style="" class="manage-column column-name" id="name-form" scope="col">Message<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-date" id="name-form" scope="col">IP<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-names" id="name-form" scope="col">Created<span class="sorting-indicator"></span></th>
        </thead>

        <tfoot>
        <tr>
            <th style="" class="manage-column column-cb check-column" scope="col"><input type="checkbox"></th>
            <th style="" class="manage-column column-s" id="name-form" scope="col">First Name<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-s" id="name-form" scope="col">Last Name<span class="sorting-indicator"></span></th>
			<th style="" class="manage-column column-s" id="name-form" scope="col">Full Name<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-s" id="name-form" scope="col">Email<span class="sorting-indicator"></span></th>
			<th style="" class="manage-column column-s" id="name-form" scope="col">Company<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-s" id="name-form" scope="col">Phone<span class="sorting-indicator"></span></th>
            <!--<th style="" class="manage-column column-s" id="name-form" scope="col">Subject<span class="sorting-indicator"></span></th>-->
            <th style="" class="manage-column column-s" id="name-form" scope="col">Message<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-date" id="name-form" scope="col">IP<span class="sorting-indicator"></span></th>
            <th style="" class="manage-column column-name" id="name-form" scope="col">Created<span class="sorting-indicator"></span></th> 
        </tfoot>
    
        <tbody id="the-list">

        <?php
			if (count($results)<1) echo '<tr class="no-items"><td colspan="3" class="colspanchange">No contact have been submit.</td></tr>';
			else {
				foreach ($results as $key => $value) {
        ?>			
    				<tr class="status-'.$row->status.'">
    					<th class="check-column" style="padding:5px 0 2px 0"><input type="checkbox" name="chk[]" value="<?php echo  $value['id']; ?>"></th>
                        <td><a href="?page=contact/index.php&add=1&edit=<?php echo  $value['id']; ?>"><?php echo  $value['first_name']; ?></a></td>
                        <td><?php echo  $value['last_name']; ?></a></td>
						<td><?php echo  $value['name']; ?></a></td>
    					<td><a href="mailto:<?php echo  $value['email']; ?>"><?php echo  $value['email']; ?></a></td>
						<td><?php echo  $value['company']; ?></a></td>
                        <td><?php echo  $value['phone']; ?></td>
                        <!--<td><?php /*echo  $value['subject']; */?></td>-->
                        <td><?php echo  $value['message']; ?></td>
                        <td><?php echo  $value['ip']; ?></td>
                        <td><?php echo  $value['created']; ?></td>
    			     </tr>
		<?php	}
			}
		?>

        </tbody>
    </table>
    <div class="paging">
    	<?php echo $paging; ?>
	</div>
    <br class="clear">
</form>