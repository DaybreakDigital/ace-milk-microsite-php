  var lat = jQuery("#lat").val();
   var lng = jQuery("#lng").val();
   var resetlat = "21.030033";
   var resetlng = "105.847092";
   
   function resetLatLng(){
      jQuery("#lat").val(resetlat);
      jQuery("#lng").val(resetlng);
      lat = resetlat;
      lng = resetlng;
      initialize(); 
   }   

    function getLocation(){
      var address = jQuery("#address_store").val()+ ' ' + jQuery("#sltDistrict option:selected").text() + ' ' + jQuery("#sltCity option:selected").text()  ;         
      if(address != ''){
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            lat = results[0].geometry.location.k;
            lng = results[0].geometry.location.A;
            jQuery("#lat").val(lat);
            jQuery("#lng").val(lng);   
            initialize(); 
          } else {
            alert('Could not find this address, please drag the marker to your address.');
          }
        });
      }else{
        jQuery("#lat").val(lat);
        jQuery("#lng").val(lng);   
        initialize(); 
      }
  }

  jQuery(document).ready(function () {
    if(typeof google != 'undefined' )
      initialize();
  });
    
  var map;
  var marker;   
  function initialize() {    
      var myLatlng = new google.maps.LatLng(lat,lng );
      var myOptions = {
          zoom: 15,
          center: myLatlng,          
          mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

      placeMarker(myLatlng);
      google.maps.event.addListener(marker, 'dragend', function (event) {
          var myLatLng = event.latLng;
          jQuery("#lat").val(myLatLng.lat());
          jQuery("#lng").val(myLatLng.lng());
      });
  }
  function placeMarker(location) {
      marker = new google.maps.Marker({
          position: location,
          animation: google.maps.Animation.DROP,
          draggable: true,
          map: map
      });
      map.setCenter(location);
      jQuery("#lat").val(map.getCenter().lat());
      jQuery("#lng").val(map.getCenter().lng());
  }

jQuery('#sltCity').change(function(evt){   
  var _idCity = jQuery(this).val();
  jQuery.get( ajaxurl, {'action' : 'get_district_from_city' , 'city': _idCity } , function(response){    
    jQuery('#sltDistrict').html(response);
  });
});