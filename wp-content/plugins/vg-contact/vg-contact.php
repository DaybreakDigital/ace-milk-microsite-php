<?php
/**
* Plugin Name: VG_Contact
* Description: 
* Version: 1.0
* Author: Vinh Giang
* Author URI: giangcamvinh@gmail.com
*/


// Plugin Activation
register_activation_hook( __FILE__, 'contact_install' );
function contact_install() {
	global $wpdb;
	$table = $wpdb->prefix."contact";
	$structure = "CREATE TABLE IF NOT EXISTS $table (
		id INT(9) NOT NULL AUTO_INCREMENT,
		first_name VARCHAR(50),
		last_name VARCHAR(50),
		name VARCHAR(255),
		email VARCHAR(50),
		company VARCHAR(255),
		phone VARCHAR(25),
		contact VARCHAR(50),
		to VARCHAR(50),
		subject VARCHAR(255),
		message TEXT,
		ip VARCHAR(50),
		created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		UNIQUE KEY id (id)
		);";
	$wpdb->query($structure);
}

// Plugin Deactivation
register_deactivation_hook( __FILE__, 'contact_uninstall' );
function contact_uninstall() {
///global $wpdb;
}

// Left Menu Button
add_action('admin_menu', 'register_contact_menu');
function register_contact_menu() {
	add_menu_page('Contact list', 'Contact list', 'add_users', dirname(__FILE__).'/index.php', '',   plugins_url('admin-icon.png', __FILE__), 59.122);
}

// Generate Subscribe Form 
add_action( 'admin_init', 'register_style_for_contact' );
function register_style_for_contact(){
	wp_register_style( 'style-contact', plugins_url( 'asset/css/admin.css', __FILE__ ), array(), 'v1.0', 'all' );
	wp_enqueue_style('style-contact');
}

add_action( 'admin_init', 'register_js_for_contact' );
function register_js_for_contact() {    
	wp_register_script( 'js-contact', plugins_url( 'asset/js/functions.js', __FILE__ ), array(), 'v1.0', 'all' );
	wp_enqueue_script('js-contact');
}

?>
